export const roleGroupKey = {
  root: "root",
};

export const colorOptions = [
  { value: "black", label: "Đen", map: "Black", hex: "#000" },
  { value: "white", label: "Trắng", map: "White", hex: "#fff" },
  { value: "gray", label: "Xám", map: "Gray", hex: "#8c8c8c" },
  { value: "red", label: "Đỏ", map: "Red", hex: "#cf1322" },
  { value: "yellow", label: "Vàng", map: "Yellow", hex: "#ffec3d" },
  { value: "orange", label: "Cam", map: "Orange", hex: "#ffa940" },
  { value: "green", label: "Xanh lá", map: "Green", hex: "#52c41a" },
  { value: "blue", label: "Xanh biển", map: "Blue", hex: "#1890ff" },
  { value: "brown", label: "Nâu", map: "Brown", hex: "#874d00" },
  { value: "purple", label: "Tím", map: "Purple", hex: "#722ed1" },
  { value: "pink", label: "Hồng", map: "Pink", hex: "#f759ab" },
];

export const genderOptions = [
  { value: "male", label: "Nam", map: "Man" },
  { value: "female", label: "Nữ", map: "Woman" },
];

export const faceAgeOptions = [
  { value: "toddler", label: "1-6 Tuổi", map: [1, 6] },
  { value: "child", label: "7-13 Tuổi", map: [7, 13] },
  { value: "teenager", label: "14-19 Tuổi", map: [14, 19] },
  { value: "young", label: "20-39 Tuổi", map: [20, 39] },
  { value: "middle", label: "40-59 Tuổi", map: [40, 59] },
  { value: "elder", label: "> 59 Tuổi", map: [60, 0] },
];

export const glassesOptions = [
  { value: "1", label: "Không", map: 1 },
  { value: "2", label: "Kính thường", map: 2 },
  { value: "3", label: "Kính râm", map: 3 },
  { value: "4", label: "Gọng đen", map: 4 },
];

export const beardOptions = [
  { value: "0", label: "Không", map: 1 },
  { value: "1", label: "Có", map: 2 },
];

export const maskOptions = [
  { value: "0", label: "Không", map: 1 },
  { value: "1", label: "Có", map: 2 },
];

export const featureAgeOptions = [
  { value: "child", label: "7-13 Tuổi", map: [7, 13] },
  { value: "middle", label: "14-59 Tuổi", map: [14, 59] },
  { value: "elder", label: "> 59 Tuổi", map: [60, 0] },
];

export const topOptions = [
  { value: "long", label: "Dài tay", map: 1 },
  { value: "short", label: "Ngắn tay", map: 2 },
];

export const botOptions = [
  { value: "pants", label: "Quần dài", map: 1 },
  { value: "shorts", label: "Quần đùi", map: 2 },
  { value: "skirt", label: "Váy", map: 3 },
];

export const bagOptions = [
  { value: "handbag", label: "Túi cầm tay", map: "CarrierBag" },
  { value: "shoulderBag", label: "Túi đeo vai", map: "ShoulderBag" },
  { value: "backpack", label: "Balo", map: "Bag" },
  { value: "none", label: "Không", map: "None" },
];

export const expressionOptions = [
  { value: "angry", label: "Tức giận", map: "Anger" },
  { value: "sad", label: "Buồn", map: "Sadness" },
  { value: "scared", label: "Lo sợ", map: "Fear" },
  { value: "suprised", label: "Ngạc nhiên", map: "Surprise" },
  { value: "happy", label: "Vui vẻ", map: "Happy" },
  { value: "calm", label: "Bình thường", map: "Calmness" },
];

export const figureOptions = [
  { value: "longhair", label: "Dài" },
  { value: "shorthair", label: "Ngắn" },
];

export const outfitOptions = [
  { value: "bag", label: "Túi xách" },
  { value: "backpack", label: "Ba lô" },
  { value: "hat", label: "Mũ" },
  { value: "dress", label: "Váy" },
];

export const defaultFaceFilter = {
  gender: "all",
  age: "all",
  glasses: "all",
  mask: "all",
  beard: "all",
  expression: "all",
};

export const defaultFeatureFilter = {
  gender: "all",
  age: "all",
  topType: "all",
  topColor: "all",
  botType: "all",
  botColor: "all",
  bag: "all",
};
