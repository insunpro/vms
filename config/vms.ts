import Env from "@core/Env";
const vmsConfig = {
  host: Env.get("VMS_HOST", "127.0.0.1"),
  port: Env.get("VMS_PORT", 9010),
  username: Env.get("VMS_USER", "admin"),
  password: Env.get("VMS_PASS", "admin123"),
  rtsp: Env.get("VMS_RTSP", 554),
};
export default vmsConfig;
