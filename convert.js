const fs = require("fs");
const path = require("path");
const { spawn } = require("child_process");

const dirPath = path.join(path.resolve(), `tmp/${"video2"}/videos`);

const files = fs.readdirSync(dirPath);

async function convert(filePath, output) {
  return new Promise((resolve, reject) => {
    let converter = spawn("ffmpeg", [
      "-i",
      filePath,
      "-c:v",
      "libx264",
      "-preset",
      "veryslow",
      "-crf",
      "18",
      output,
    ]);
    converter.stderr.on("data", (chunk) => {
      console.log(chunk.toString());
    });
    converter.stderr.on("end", () => resolve(true));
  });
}

async function start() {
  for (let i in files) {
    if (path.extname(files[i]) === ".mp4") continue;

    let file = path.join(dirPath, files[i]);
    let out = files[i].replace(".avi", ".mp4");
    if (fs.existsSync(out)) continue;

    await convert(file, out);
  }
}

start();
