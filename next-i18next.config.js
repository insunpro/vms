const I18NextHttpBackend = require('i18next-http-backend/cjs')

let config = {
  defaultNS: "common",
  ns: ['common', 'pages', 'buttons', 'errors', 'menu', 'messages'],
  debug: false,
  i18n: {
    defaultLocale: 'vi',
    locales: ['en', 'vi'],
  },

  partialBundledLanguages: true,
  serializeConfig: false,
  react: {
    useSuspense: false,
  },
};

if (process.browser) {
  config = {
    ...config,
    backend: {
      loadPath: `/locales/{{lng}}/{{ns}}.json`,
      allowMultiLoading: true,
      crossDomain: true,
      withCredentials: false,
    },
    use: [I18NextHttpBackend]

  }
}

module.exports = config