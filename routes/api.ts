import Route from "@core/Routes";
const ExtendMiddleware = require("@app/Middlewares/ExtendMiddleware");
const AuthApiMiddleware = require("@app/Middlewares/AuthApiMiddleware");
//const { permission, permissionResource, permissionMethod } = require('@app/Middlewares/PermissionMiddleware');

const multer = require('multer')
const upload = multer()

Route.group(() => {
  // ---------------------------------- Auth Routes ---------------------------------------//
  Route.post("/login", "AuthController.login").name("auth.login");
  Route.post("/forgotPassword", "AuthController.forgotPassword").name(
    "auth.forgotPassword"
  );
  Route.get("/checkToken/:token", "AuthController.checkToken").name(
    "auth.checkToken"
  );
  Route.post("/resetPassword", "AuthController.resetPassword").name(
    "auth.resetPassword"
  );

  Route.get("/img2", "DeviceController.forwardVideoImage").name("device.forwardVideoImage");
  Route.get("/img", "DeviceController.forwardImage").name("device.forwardImage");

  // ---------------------------------- End Auth Routes -----------------------------------//

  // ---------------------------------- Route Routes ---------------------------------------//
  Route.get("/routes", "RouteController.index").name("routes.index");
  // ---------------------------------- End Route Routes -----------------------------------//
  Route.group(() => {
    Route.post("/changePassword", "AuthController.changePassword").name(
      "auth.changePassword"
    );
    Route.post("/logout", "AuthController.logout").name("auth.logout");

    // ---------------------------------- Admin Routes ---------------------------------------//
    Route.resource("/admins", "AdminController").name("admins");
    Route.post("/admins/loginAs", "AdminController.loginAs").name(
      "admins.loginAs"
    );
    // ---------------------------------- End Admin Routes -----------------------------------//

    // ---------------------------------- Admin Routes ---------------------------------------//
    Route.resource("/users", "UserController").name("users");
    // ---------------------------------- End Admin Routes -----------------------------------//

    // ---------------------------------- Post Routes ---------------------------------------//
    Route.post("/analysis/find", "AnalysisController.analysis").name("analysis.analysis");
    Route.post("/analysis/smart", "AnalysisController.smartVideo").name("analysis.smartVideo");
    Route.post("/analysis/photo", "AnalysisController.smartPhoto").name("analysis.smartPhoto").middleware([upload.any()]);
    // ---------------------------------- End Post Routes -----------------------------------//

    // ---------------------------------- Device Routes ---------------------------------------//
    Route.post("/scan/face", "DeviceController.faceScan").name("device.faceScan");
    Route.post("/scan/feature", "DeviceController.featureScan").name("device.featureScan");
    Route.post("/scan/video", "DeviceController.videoScan").name("device.videoScan");
    Route.post("/scan/image", "DeviceController.imageScan").name("device.imageScan").middleware([upload.any()]);
    Route.post("/scan/next", "DeviceController.nextScan").name("device.nextScan");
    Route.get("/webrtc/sdp", "DeviceController.webrtcSdp").name('webrtc.webrtcSdp')
    Route.get("/webrtc/codec", "DeviceController.webrtcCodec").name('webrtc.webrtcCodec')
    Route.get("/device/playback", "DeviceController.getPlayback").name("device.getPlayback");
    Route.get("/device/list", "DeviceController.getList").name("device.getList");
    Route.get("/device/streams", "DeviceController.getStreams").name("device.getStreams");

  }).middleware([AuthApiMiddleware]);

  Route.post("/analysis/test", "AnalysisController.test").name("analysis.test").middleware([upload.any()]);

})
  .middleware([ExtendMiddleware])
  .name("api")
  .prefix("/api/v1");
