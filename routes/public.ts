import Route from '@core/Routes'

Route.get("/", "pages/common/login").name("frontend.default")
Route.get("/login", "pages/common/login").name("frontend.login")
Route.get("/forgot-password", "pages/common/forgotPassword").name("frontend.admin.forgotPassword")
Route.get("/reset-password/:token", "pages/common/resetPassword").name("frontend.admin.resetPassword")
