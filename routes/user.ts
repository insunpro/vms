import Route from '@core/Routes'
const AuthAdminMiddleware = require('@app/Middlewares/AuthAdminMiddleware')

Route.group(() => {
  Route.get("/", "pages/user/dashboard").name("home").sidebar('dashboard.index')
  {
    let name = 'dashboard'
    Route.get(`/${name}`, `pages/user/${name}`).name(`${name}.index`).sidebar(`${name}.index`)
  }

  {
    let name = 'analysis'
    Route.get(`/${name}`, `pages/user/${name}`).name(`${name}.index`).sidebar(`${name}.index`)
  }

  {
    let name = 'cameras'
    Route.get(`/${name}`, `pages/user/${name}`).name(`${name}.index`).sidebar(`${name}.index`)
  }

  {
    let name = 'videos'
    Route.get(`/${name}`, `pages/user/${name}`).name(`${name}.index`).sidebar(`${name}.index`)
  }


}).name("frontend.user").prefix("/user").middleware([AuthAdminMiddleware])
