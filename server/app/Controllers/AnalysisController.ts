import BaseController from "./BaseController";
import _debug from "debug";
import _ from "lodash";
import path from "path";
import fs from "fs";
import ApiException from "../Exceptions/ApiException";
import fetch from "node-fetch";
import FormData from "form-data";

export default class AnalysisController extends BaseController {
  async analysis() {
    let inputs = this.request.all();
    let { target, gender, figure, color, outfit } = inputs;
    if (!target || !target.type) return [];
    let video = "video1";
    if (target.type === "video") {
      video = target.data.name;
    }
    let dataFile = path.join(path.resolve(), `tmp/${video}/data.json`);
    if (!fs.existsSync(dataFile)) return [];

    let data = JSON.parse(fs.readFileSync(dataFile, { encoding: "utf-8" }));

    let times = _.get(data, "time", {});

    let objectIds = Object.keys(times);

    let attrs = Object.keys(_.get(data, "attribute", {}));

    if (gender && gender.length == 1) {
      let attribute = _.get(data, "attribute.gender", []);
      if (gender[0] === "male") {
        objectIds = _.intersection(attribute, objectIds);
      }
      if (gender[0] === "female") {
        objectIds = _.difference(objectIds, attribute);
      }
    }

    if (figure && figure.length == 1) {
      let attribute = _.get(data, "attribute.hair", []);
      if (figure[0] === "shorthair") {
        objectIds = _.intersection(attribute, objectIds);
      }
      if (figure[0] === "longhair") {
        objectIds = _.difference(objectIds, attribute);
      }
    }

    if (color && color.length) {
      color.map((value) => {
        let subattrs = attrs.filter((val) => val.indexOf(value) !== -1);
        let attributes = [];
        subattrs.forEach((value) => {
          let attribute = _.get(data, `attribute.${value}`, []);
          attributes = _.concat(attribute, attributes);
        });
        objectIds = _.intersection(attributes, objectIds);
      });
    }

    if (outfit && outfit.length) {
      outfit.forEach((value) => {
        let attribute = _.get(data, `attribute.${value}`, []);
        objectIds = _.intersection(attribute, objectIds);
      });
    }
    let attr = _.get(data, "attribute.gender", []);

    let dirPath = _.get(data, "path_ids");
    let objects = [];
    objectIds.forEach((objId) => {
      if (times[objId]) {
        let relatePath = path.join(`tmp/${video}`, dirPath, objId);
        let srcPath = path.join(path.resolve(), relatePath);
        let files = fs.readdirSync(srcPath);

        objects.push({
          id: objId,
          time: times[objId],
          filePath: relatePath,
          files,
        });
      }
    });

    return objects;
  }

  async smartVideo() {
    let inputs = this.request.all();
    let { target, gender, figure, color, outfit } = inputs;
    if (!target || !target.type) return "";
    let video = "video1";
    if (target.type === "video") {
      video = target.data.name;
    }
    let dataFile = path.join(path.resolve(), `tmp/${video}/data.json`);
    if (!fs.existsSync(dataFile)) return "";

    let data = JSON.parse(fs.readFileSync(dataFile, { encoding: "utf-8" }));

    let dataVideo = _.get(data, "video", {});

    let defaultVideo = dataVideo["all"];
    let relatePath = path.join(`tmp/${video}`, defaultVideo);

    return relatePath;
  }

  async test() {
    let inputs = this.request.all();
    let files = this.request.files;
    console.log(inputs);
    console.log(files);

    return true;
  }

  async smartPhoto() {
    let aiApi = "http://ai.demo.mqsolutions.vn/api/v1/extract_feature_person";
    // let aiApi = "http://localhost:3333/api/v1/analysis/test";

    let inputs = this.request.all();
    let files = this.request.files;

    if (files.length !== 1) throw new ApiException(400, "require.imageFile");

    let form = new FormData();
    // form.append("Content-Type", "application/octet-stream");
    form.append("image", files[0].buffer, {
      knownLength: files[0].size,
      filename: files[0].originalname,
    });

    let res;
    try {
      res = await fetch(aiApi, {
        method: "POST",
        body: form,
      });
    } catch (e) {
      console.error(e);
      return e;
    }

    return res.json();
  }
}
