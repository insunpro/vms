import BaseController from "./BaseController";
import CategoryModel from "@app/Models/CategoryModel";
import ApiException from "@app/Exceptions/ApiException";
import TemplateEmail from "@config/email_templates";

export default class Controller extends BaseController {
  Model: typeof CategoryModel = CategoryModel;

  async index() {
    const { auth } = this.request;
    let inputs = this.request.all();
    return this.Model.queryWithParent().getForGridTable(inputs);
  }

  async store() {
    const { auth } = this.request;
    let inputs = this.request.all();
    const allowFields = {
      name: "string!",
      description: "string",
      parentId: "number",
    };
    let params = this.validate(inputs, allowFields, { removeNotAllow: true });

    let nameExist = await this.Model.findExist(params.name, "name");
    if (nameExist) throw new ApiException(6007, "Name already exists!");

    let result = await this.Model.insertOne(params);
    return result;
  }

  async update() {
    let inputs = this.request.all();
    const allowFields = {
      id: "number!",
      name: "string!",
      description: "string",
      parentId: "number",
    };
    let params = this.validate(inputs, allowFields, { removeNotAllow: true });
    const { id } = params;
    delete params.id;

    let exist = await this.Model.getById(id);
    if (!exist) throw new ApiException(6006, "Category doesn't exists!");

    let result = await this.Model.updateOne(id, params);

    return {
      result,
      old: exist,
    };
  }

  async destroy() {
    const { auth } = this.request;
    let params = this.request.all();
    let id = params.id;
    if (!id) throw new ApiException(9996, "ID is required!");
    await this.Model.query().where("id", id).delete();
    return {
      message: `Delete successfully`,
    };
  }

  async delete() {
    const { auth } = this.request;
    const allowFields = {
      ids: ["number!"],
    };

    const inputs = this.request.all();
    let params = this.validate(inputs, allowFields);

    let categories = await this.Model.query().whereIn("id", params.ids);

    for (let user of categories) {
      await user.$query().delete();
    }
    return {};
  }
}
