import BaseController from "./BaseController";
import ApiException from "@app/Exceptions/ApiException";
import moment from "moment-timezone";
import { DahuaRpc } from "@mq-services/dahua-api";
import http from "http";
import { Response } from "express";
import fetch from "node-fetch";
import _ from "lodash";
import path from "path";
import fs from "fs";
import {
  bagOptions,
  beardOptions,
  botOptions,
  colorOptions,
  expressionOptions,
  faceAgeOptions,
  featureAgeOptions,
  genderOptions,
  glassesOptions,
  maskOptions,
  topOptions,
} from "@root/config/constant";
import WebRtcService from "../Services/WebRtc";
import StreamService from "../Services/Stream";
import sizeOf from "image-size";
import vmsConfig from "@root/config/vms";

let sessionID = null;

const cachedFinder = new Map();
const faceFinders = [];
const featureFinders = [];

const DEFAULT_LIMIT_SCAN = 50;
export default class DeviceController extends BaseController {
  async getList() {
    let inputs = this.request.all();
    const allowFields = {};
    let params = this.validate(inputs, allowFields, { removeNotAllow: true });

    let connection = await DahuaRpc.createConnection(
      {
        ...vmsConfig,
      },
      true
    );

    let data = await connection.AsyncDeviceManager.getDeviceInfoEx();

    return data.params?.info || [];
  }

  async getStreams() {
    let data = await this.getList();
    let config = { ...vmsConfig };

    let streams = {};
    data.map((device) => {
      device.channels.map((channel) => {
        let channelNo = Number(channel.logicChannel) + 1;
        streams["cam-" + channelNo] = {
          // on_demand: false,
          // disable_audio: true,
          name: "cam-" + channelNo,
          url: `rtsp://${config.username}:${config.password}@192.168.6.168:9010/cam/realmonitor?channel=${channelNo}&subtype=0`,
          info: {},
        };
      });
    });
    return streams;
  }

  async faceScan() {
    let inputs = this.request.all();
    const allowFields = {
      channels: ["number!"],
      startTime: "string!",
      endTime: "string!",
      filters: "object",
    };
    let params = this.validate(inputs, allowFields, { removeNotAllow: true });

    let filterAge = faceAgeOptions.find(
      (val) => val.value === params.filters.age
    );
    let filterGender = genderOptions.find(
      (val) => val.value === params.filters.gender
    );
    let filterGlasses = glassesOptions.find(
      (val) => val.value === params.filters.glasses
    );
    let filterMask = maskOptions.find(
      (val) => val.value === params.filters.mask
    );
    let filterBeard = beardOptions.find(
      (val) => val.value === params.filters.beard
    );
    let filterExpression = expressionOptions.find(
      (val) => val.value === params.filters.expression
    );
    const defaultFilter = {
      Age: filterAge ? filterAge.map : [0, 200],
      Beard: filterBeard ? filterBeard.map : 0,
      Emotion: filterExpression ? [filterExpression.map] : ["*"],
      Glasses: filterGlasses ? filterGlasses.map : 0,
      ImageType: "Small",
      IsStranger: 0,
      Mask: filterMask ? filterMask.map : 0,
      Sex: filterGender ? filterGender.map : "*",
      Orders: [{ Field: "RecNo", Type: "Ascent" }],
    };

    const personFilter = {
      Age: defaultFilter.Age,
      Beard: defaultFilter.Beard,
      Emotion: defaultFilter.Emotion,
      FrequencyAlarmType: [-1, 0, 1],
      Glasses: defaultFilter.Glasses,
      Mask: defaultFilter.Mask,
    };

    const condition = {
      Channels: [...params.channels],
      Class: ["*"],
      Events: ["*"],
      Types: ["jpg"],
      StartTime: moment(params.startTime)
        .utcOffset(420)
        .format("YYYY-MM-DD HH:mm:ss"),
      EndTime: moment(params.endTime)
        .utcOffset(420)
        .format("YYYY-MM-DD HH:mm:ss"),
      DB: {
        "FaceDetectionRecord Filter": defaultFilter,
        FaceRecognitionRecordFilter: {
          ImageType: "Small",
          Orders: [{ Field: "RecNo", Type: "Descent" }],
          Person: personFilter,
          SimilaryRange: [0, 100],
        },
      },
    };

    let connection = await DahuaRpc.createConnection(
      {
        ...vmsConfig,
      },
      true
    );

    // precheck
    if (faceFinders.length) {
      for (let faceFinder of faceFinders) {
        if (cachedFinder.has(faceFinder)) {
          await this.destroyFinder(faceFinder);
        }
      }
    }

    let res1 = await connection.MediaFileFind.factoryCreate(null);
    console.log("res1", res1);
    sessionID = connection.session;
    let res2 = await connection.MediaFileFind.findFile(res1.result, condition);
    console.log("res2", res2);
    let res3 = await connection.MediaFileFind.getCountVariedByTask(
      res1.result,
      "ByDay"
    );
    console.log("res3", res3);
    // let res4 = await connection.TaskManager.getTaskInfo()
    let res5 = await connection.MediaFileFind.getCountVaried(
      res1.result,
      "ByDay"
    );
    console.log("res5", res5);
    let res6 = await connection.MediaFileFind.findNextFile(
      res1.result,
      DEFAULT_LIMIT_SCAN
    );
    console.log("res6", res6.params?.infos?.length);
    // let res99 = await connection.MediaFileFind.destroy(res1.result);
    // console.log("res99", res99);
    cachedFinder.set(res1.result + "", connection);
    faceFinders.push(res1.result);

    return {
      session: sessionID,
      data: res6.params?.infos || [],
      total: res5.params?.Total,
      finderId: res1.result,
    };
  }

  async featureScan() {
    let inputs = this.request.all();
    const allowFields = {
      channels: ["number!"],
      startTime: "string!",
      endTime: "string!",
      filters: "object",
    };
    let params = this.validate(inputs, allowFields, { removeNotAllow: true });

    let filterAge = featureAgeOptions.find(
      (val) => val.value === params.filters.age
    );
    let filterGender = genderOptions.find(
      (val) => val.value === params.filters.gender
    );
    let filterCoatType = topOptions.find(
      (val) => val.value === params.filters.topType
    );
    let filterCoatColor = colorOptions.find(
      (val) => val.value === params.filters.topColor
    );
    let filterTrousersType = botOptions.find(
      (val) => val.value === params.filters.botType
    );
    let filterTrousersColor = colorOptions.find(
      (val) => val.value === params.filters.botColor
    );
    let filterBag = bagOptions.find((val) => val.value === params.filters.bag);

    const defaultFilter = {
      Age: filterAge ? filterAge.map : [0, 0],
      CoatColor: filterCoatColor ? [filterCoatColor.map] : ["All"],
      CoatType: filterCoatType ? [filterCoatType.map] : [0],
      TrousersColor: filterTrousersColor ? [filterTrousersColor.map] : ["All"],
      TrousersType: filterTrousersType ? [filterTrousersType.map] : [0],
      HasHat: 0,
      Sex: filterGender ? filterGender.map : "*",
    };

    if (filterBag) {
      defaultFilter["BagType"] = [filterBag.map];
    }

    const condition = {
      Channels: [...params.channels],
      Class: ["*"],
      Events: ["*"],
      Types: ["jpg"],
      StartTime: moment(params.startTime)
        .utcOffset(420)
        .format("YYYY-MM-DD HH:mm:ss"),
      EndTime: moment(params.endTime)
        .utcOffset(420)
        .format("YYYY-MM-DD HH:mm:ss"),
      DB: {
        HumanTraitRecordFilter: {
          HumanAttributes: defaultFilter,
        },
      },
    };

    let connection = await DahuaRpc.createConnection(
      {
        ...vmsConfig,
      },
      true
    );

    // precheck
    if (featureFinders.length) {
      for (let featureFinder of featureFinders) {
        if (cachedFinder.has(featureFinder)) {
          await this.destroyFinder(featureFinder);
        }
      }
    }

    let res1 = await connection.MediaFileFind.factoryCreate(null);
    sessionID = connection.session;
    let res2 = await connection.MediaFileFind.findFile(res1.result, condition);
    console.log("res2", res2);
    let res3 = await connection.MediaFileFind.getCountVariedByTask(
      res1.result,
      "ByDay"
    );
    console.log("res3", res3);
    // let res4 = await connection.TaskManager.getTaskInfo()
    let res5 = await connection.MediaFileFind.getCountVaried(
      res1.result,
      "ByDay"
    );
    console.log("res5", res5.params);
    let res6 = await connection.MediaFileFind.findNextFile(
      res1.result,
      DEFAULT_LIMIT_SCAN
    );
    console.log("res6", res6.params?.infos?.length);
    // let res99 = await connection.MediaFileFind.destroy(res1.result);
    // console.log("res99", res99);
    cachedFinder.set(res1.result + "", connection);
    featureFinders.push(res1.result);
    return {
      session: sessionID,
      data: res6.params?.infos || [],
      total: res5.params?.Total,
      finderId: res1.result,
    };
  }

  async videoScan() {
    let inputs = this.request.all();
    const allowFields = {
      channels: ["number!"],
      startTime: "string!",
      endTime: "string!",
      filters: "object",
    };
    let params = this.validate(inputs, allowFields, { removeNotAllow: true });
    console.log(params.startTime, params.endTime);

    const condition = {
      Channels: [...params.channels],
      Events: ["*"],
      Types: ["dav"],
      Flags: ["*"],
      Mode: "Event",
      StartTime: moment(params.startTime)
        .utcOffset(420)
        .format("YYYY-MM-DD HH:mm:ss"),
      EndTime: moment(params.endTime)
        .utcOffset(420)
        .format("YYYY-MM-DD HH:mm:ss"),
      VideoStream: "Main",
    };

    console.log(condition);

    let result = await this.findFiles(condition, 1000);

    return {
      session: sessionID,
      data: result.params?.infos || [],
      total: result.params?.found || 0,
      finderId: result.finderId,
    };
  }

  async nextScan() {
    const inputs = this.request.all();
    const allowFields = {
      finderId: "string!",
      current: "number!",
    };
    const { finderId, current } = this.validate(inputs, allowFields);

    let result = await this.findNextFiles(
      finderId,
      current,
      DEFAULT_LIMIT_SCAN
    );
    return {
      data: _.get(result, "params.infos", []),
      total: _.get(result, "params.found", 0),
      finderId: result.finderId,
    };
  }

  async getPlayback() {
    let inputs = this.request.all();
    console.log(inputs);
    if (inputs.times && inputs.times.length) {
      try {
        inputs.times = inputs.times.map((val) => JSON.parse(val));
      } catch (e) {}
    }
    const allowFields = {
      channel: "number!",
      times: [
        {
          startTime: "string!",
          endTime: "string!",
        },
      ],
    };
    let { channel, times } = this.validate(inputs, allowFields, {
      removeNotAllow: true,
    });

    const config = { ...vmsConfig };
    const INPUT_FORMAT = "YYYY-MM-DD HH:mm:ss";
    let firstStart, lastEnd;
    console.log("offtime server", moment().utcOffset());
    const offsetTime = moment().utcOffset();
    let urls = times.map((time, index) => {
      let startTime: any = moment(time.startTime, INPUT_FORMAT, true);
      let endTime: any = moment(time.endTime, INPUT_FORMAT, true);
      if (!startTime.isValid() || !endTime.isValid()) {
        throw new ApiException(400, "Wrong date!");
      }
      startTime = startTime
        // .add(offsetTime - 420, "minutes")
        // .utcOffset(420)
        .format("YYYY_MM_DD_HH_mm_ss");
      endTime = endTime
        // .add(offsetTime - 420, "minutes")
        // .utcOffset(420)
        .format("YYYY_MM_DD_HH_mm_ss");
      if (index === 0) firstStart = startTime;
      if (index === times.length - 1) lastEnd = endTime;
      console.log({ startTime, endTime });
      return `rtsp://${config.username}:${config.password}@${config.host}:${config.rtsp}/cam/playback?channel=${channel}&starttime=${startTime}&endtime=${endTime}`;
    });

    const outDir = "tmp/streams";
    const key = `c${channel}-${firstStart}-${lastEnd}`;
    let stream = await StreamService.getHlsStream(key, urls, outDir);
    return `/${outDir}/${key}/index.m3u8`;
  }

  async imageScan() {
    let inputs = this.request.all();
    const allowFields = {
      channels: ["number!"],
      startTime: "string!",
      endTime: "string!",
      image: "string!",
      type: "number!",
    };
    let params = this.validate(inputs, allowFields, { removeNotAllow: true });
    let { channels, startTime, endTime, image, type } = params;

    const imageData = Buffer.from(image.split(",")[1], "base64");
    const imageSize = sizeOf(imageData);

    const objectType = type === 1 ? 1 : 0;

    const INPUT_FORMAT = "YYYY-MM-DD HH:mm:ss";
    startTime = moment(startTime).utcOffset(420);
    endTime = moment(endTime).utcOffset(420);

    let connection = await DahuaRpc.createConnection(
      {
        ...vmsConfig,
      },
      true
    );

    const res1 = await connection.FaceRecognitionServer.getDetectToken();

    const res2 = await connection.FaceRecognitionServer.attachDetectState([
      res1.params.token,
    ]);

    const res3 = await connection.FaceRecognitionServer.detectMultiFace(
      imageData,
      {
        BigID: 0,
        Offset: 0,
        Length: imageData.length,
        Width: imageSize.width,
        Height: imageSize.height,
        //@ts-ignore
        // Center: [4021, 4096],
      },
      objectType,
      res1.params.token
    );
    const res41 = await new Promise<any[]>((r) => {
      connection.CgiNotify.subscribe("client.notifyDetectFaceState", (data) => {
        console.log(data);
        if (data["state"]["Progress"] == 100) {
          r(data["state"]["CurrentCount"]);
        }
      });
    });
    console.log("res41", res41);

    const res4 = await connection.FaceRecognitionServer.detachDetectState([
      res1.params.token,
    ]);

    if (_.isEmpty(res41["ObjectTypes"])) {
      throw new ApiException(6001, "can not detect");
    }
    // return [];
    const smallIDS = res41["SmallPicIDs"];

    const options = {
      Ordered: 2,
      Similarity: 85,
      ResultMode: 1,
      AdvancedSearch: false,
      QueryMode: 1,
    };

    const conditions = {
      condition: {
        StartTime: startTime.format(INPUT_FORMAT),
        EndTime: endTime.format(INPUT_FORMAT),
        Range: ["HistoryDB"],
      },
    };

    const res5 = await connection.FaceRecognitionServer.startMultiPersonFind(
      channels,
      smallIDS,
      objectType,
      options,
      conditions.condition,
      conditions
    );

    const res6 = await connection.FaceRecognitionServer.attachFindResultHistory(
      res5.params.token
    );

    const res7 = await new Promise<any[]>((r) => {
      let allData = [];

      connection.CgiNotify.subscribe(
        "client.notifyFindHistoryResult",
        (data) => {
          if (data["Detail"] && data["Detail"].length) {
            data["Detail"].forEach((detail) => {
              if (!detail) return;

              allData = [...allData, ...detail["Candidates"]];
            });
          }
          if (data["Progress"] == 100) {
            r(allData);
          }
        }
      );
    });

    const orderedData = _.orderBy(
      res7,
      (val) => {
        return new Date(val["Time"].replace(/-/g, "/")).getTime();
      },
      "asc"
    );

    return {
      total: orderedData.length,
      data: orderedData,
    };
  }

  async forwardImage() {
    const inputs = this.request.all();
    const allowFields = {
      path: "string!",
    };

    let params = this.validate(inputs, allowFields);
    let cachePath = path.join(path.resolve(), "tmp/cached");
    let cacheImg = path.join(cachePath, params.path);
    if (!fs.existsSync(path.dirname(cacheImg))) {
      fs.mkdirSync(path.dirname(cacheImg), { recursive: true });
    }
    const config = { ...vmsConfig };

    if (!fs.existsSync(cacheImg)) {
      let cookie = _.get(this.request, "headers.cookie", "");
      let session = cookie.split("SessionID=")[1];
      let fetchUrl = `http://${config.host}:${config.port}${params.path}`;

      console.log("fetchUrl:", fetchUrl);

      let res = await fetch(fetchUrl, {
        headers: {
          cookie: `secure; SessionID=${session}`,
        },
        method: "GET",
      });
      if (!res.ok) {
        throw new ApiException(res.status, res.statusText);
      }
      let data = await res.buffer();
      fs.writeFileSync(cacheImg, data);
    }

    this.response.sendFile(cacheImg);
    return false;
  }

  async forwardVideoImage() {
    const inputs = this.request.all();
    const allowFields = {
      path: "string!",
    };

    let params = this.validate(inputs, allowFields);

    params = Buffer.from(params.path, "base64");
    params = params.toString();
    try {
      params = JSON.parse(params);
    } catch (e) {
      throw new ApiException(400, "bad params");
    }

    let cachePath = path.join(path.resolve(), "tmp/cached");
    let imgName = `${params.time}.jpg`;
    let cacheImg = path.join(cachePath, params.path, imgName);
    if (!fs.existsSync(path.dirname(cacheImg))) {
      fs.mkdirSync(path.dirname(cacheImg), { recursive: true });
    }
    const config = { ...vmsConfig };
    if (!fs.existsSync(cacheImg)) {
      let cookie = _.get(this.request, "headers.cookie");
      let session = cookie.split("SessionID=")[1];
      let fetchUrl = `http://${config.host}:${config.port}/simplePic${params.path}/${imgName}?SessionID=${session}`;
      fetchUrl = fetchUrl.replaceAll(" ", "%20");
      let res = await fetch(fetchUrl, {
        headers: {
          cookie: `secure; SessionID=${session}`,
        },
        method: "GET",
      });
      if (!res.ok) {
        throw new ApiException(res.status, res.statusText);
      }
      let data = await res.buffer();
      fs.writeFileSync(cacheImg, data);
    }

    this.response.sendFile(cacheImg);
    return false;
  }

  // ### UTIL FUNCTION ###
  async findFiles(condition, limit = DEFAULT_LIMIT_SCAN) {
    let connection = await DahuaRpc.createConnection(
      {
        ...vmsConfig,
      },
      true
    );

    let res1 = await connection.MediaFileFind.factoryCreate(null);
    sessionID = connection.session;
    let res2 = await connection.MediaFileFind.findFile(res1.result, condition);
    console.log("res2", res2);

    let res6 = await connection.MediaFileFind.findNextFile(res1.result, limit);
    console.log("res6", res6.params?.infos?.length);
    let res99 = await connection.MediaFileFind.destroy(res1.result);
    console.log("res99", res99);

    // cachedFinder.set(res1.result, condition);
    // res6["finderId"] = res1.result;

    return res6;
  }

  async findNextFiles(finderId, current, limit = DEFAULT_LIMIT_SCAN) {
    if (!cachedFinder.has(finderId)) {
      return {
        finderId: null,
        params: {
          infos: [],
          found: 0,
        },
      };
    }

    //set off set
    let res1 = await cachedFinder
      .get(finderId)
      .MediaFileFind.setQueryResultOptions(Number(finderId), current);
    console.log("findNextFiles.res1", res1);
    let result = await cachedFinder
      .get(finderId)
      .MediaFileFind.findNextFile(Number(finderId), limit);
    console.log("findNextFiles", result);
    result.finderId = finderId;
    return result;
  }

  async destroyFinder(finderId) {
    if (cachedFinder[finderId]) {
      await cachedFinder[finderId].MediaFileFind.destroy(finderId);
      cachedFinder.delete(finderId);
    }
  }

  // ### WEB RTC ###
  async webrtcSdp() {
    let inputs = this.request.all();
    const allowFields = {
      host: "string!",
      channel: "string!",
      sdp: "string!",
    };
    const { host, channel, sdp } = this.validate(inputs, allowFields);

    return await WebRtcService.getRemoteSdp(host, channel, sdp);
  }

  async webrtcCodec() {
    let inputs = this.request.all();
    const allowFields = {
      host: "string!",
      channel: "string!",
    };
    const { host, channel } = this.validate(inputs, allowFields);

    return await WebRtcService.getCodecInfo(host, channel);
  }
}
