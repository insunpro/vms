import BaseController from "./BaseController";
import UserModel from "@app/Models/UserModel";
import ApiException from "@app/Exceptions/ApiException";
import TemplateEmail from "@config/email_templates";

export default class AdminController extends BaseController {
  Model: typeof UserModel = UserModel;

  async index() {
    const { auth } = this.request;
    let inputs = this.request.all();
    let query = this.Model.query();

    return await query.getForGridTable(inputs);
  }

  async store() {
    const { auth } = this.request;
    let inputs = this.request.all();
    const allowFields = {
      firstName: "string!",
      lastName: "string!",
      username: "string!",
      password: "string!",
      email: "string!",
    };
    let params = this.validate(inputs, allowFields, { removeNotAllow: true });

    let username = params.username.replace(
      /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g,
      ""
    );
    let usernameExist = await this.Model.findExist(username, "username");
    if (usernameExist) throw new ApiException(6007, "Username already exists!");

    let emailExist = await this.Model.findExist(params.email, "email");
    if (emailExist) throw new ApiException(6021, "Email already exists!");

    let variables = {
      username: params.username,
      password: params.password,
    };
    if (params["password"]) {
      params["password"] = await this.Model.hash(params["password"]);
    }

    params = {
      ...params,
      createdBy: auth.id,
    };
    let result = await this.Model.insertOne(params);
    delete result["password"];
    let { subject, content } = TemplateEmail["login"];
    return result;
  }

  async update() {
    let inputs = this.request.all();
    const allowFields = {
      id: "number!",
      firstName: "string!",
      lastName: "string!",
      email: "string!",
    };
    let params = this.validate(inputs, allowFields, { removeNotAllow: true });
    const { id } = params;
    delete params.id;
    let exist = await this.Model.getById(id);
    if (!exist) throw new ApiException(6006, "User doesn't exists!");
    if (exist.email !== params.email) {
      let emailExist = await this.Model.findExist(params.email, "email");
      if (emailExist) throw new ApiException(6021, "Email already exists!");
    }
    let result = await this.Model.updateOne(id, params);
    delete result["password"];
    return {
      result,
      old: exist,
    };
  }

  async destroy() {
    const { auth } = this.request;
    let params = this.request.all();
    let id = params.id;
    if (!id) throw new ApiException(9996, "ID is required!");
    if ([id].includes(auth.id))
      throw new ApiException(6022, "You can not remove your account.");
    let user = await this.Model.query().where("id", params.id).first();
    await user.$query().delete();
    return {
      message: `Delete successfully`,
      old: user,
    };
  }

  async delete() {
    const { auth } = this.request;
    const allowFields = {
      ids: ["number!"],
    };
    const inputs = this.request.all();
    let params = this.validate(inputs, allowFields);
    if (params.ids.includes(auth.id))
      throw new ApiException(6022, "You can not remove your account.");
    let users = await this.Model.query().whereIn("id", params.ids);
    for (let user of users) {
      await user.$query().delete();
    }
    return {
      old: {
        usernames: (users || []).map((user) => user.username).join(", "),
      },
    };
  }
}
