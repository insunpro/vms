import BaseModel from "./BaseModel";

class CategoryModel extends BaseModel {
  static tableName = "categories";

  //fields
  id: number;
  code: string;
  name: string;
  description: any;

  static async getChildren(id, includeMe = true) {
    let childrens = await CategoryModel.query().where("parentIds", "@>", id);
    let childrenIds = childrens.map((children) => children.id);
    if (includeMe) childrenIds.push(id);
    return childrenIds;
  }
  static queryWithParent() {
    return CategoryModel.query()
      .select("categories.*", "parent.name as parentName")
      .leftJoin("categories as parent", "categories.parentId", "parent.id");
  }
}

export default CategoryModel;
