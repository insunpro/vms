import BaseModel from './BaseModel'

class CustomerModel extends BaseModel {
  static tableName = "customers"

  //fields
  id: number;
  code: string;
  name: string;
  description: any;



}

export default CustomerModel
