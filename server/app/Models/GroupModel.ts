import BaseModel from './BaseModel'

class GroupModel extends BaseModel {
  static tableName = "groups"

  //fields
  id: number;
  code: string;
  name: string;
  description: any;

  static async getChildren(id, includeMe = true) {
    let childrens = await GroupModel.query().where('parentIds', '@>', id)
    let childrenIds = childrens.map(children => children.id);
    if (includeMe) childrenIds.push(id);
    return childrenIds;
  }

}

export default GroupModel
