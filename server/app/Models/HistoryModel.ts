import BaseModel from './BaseModel'

class HistoryModel extends BaseModel {
  static tableName = "histories"

  //fields
  id: number;
  phoneNumber: string;
  action: string;
  reason: string;
  type: number;
  bathId: string;
  groupId: number;
  createdAt: Date;
  updatedAt: Date;
}

export default HistoryModel
