import BaseModel from './BaseModel'

class PostModel extends BaseModel {
  static tableName = "posts"

  //fields
  id: number;
  code: string;
  name: string;
  description: any;



}

export default PostModel
