import BaseModel from './BaseModel'
import moment from 'moment';
import { raw } from 'objection'

const units = {
  second: 'YYYY-MM-DD HH24:MI:SS',
  minute: 'YYYY-MM-DD HH24:MI',
  hour: 'YYYY-MM-DD HH24:00',
  day: 'YYYY-MM-DD',
  month: 'YYYY-MM',
  year: 'YYYY',
}
class QueueLogModel extends BaseModel {
  static tableName = "queue_logs"

  //fields
  id: number;
  requestTime: Date;
  resonseTime: Date;
  quantity: number;
  result: number;
  createdAt: Date;
  updatedAt: Date;

  static async statsQuantity({ startTime, endTime, unit, botId }: {
    startTime: Date,
    endTime: Date,
    botId?: string,
    unit: "second" | "minute" | "hour" | "day" | "month" | "year"
  }) {

    let query = this.query().select([
      raw('COUNT(ID) as "request"'),
      raw('SUM(quantity) as "numbers"'),
      raw('SUM(quantity) as "numbers"'),
      raw('AVG(EXTRACT(epoch FROM "responseTime" - "requestTime")) as "avgTime"'),
      raw(`TO_CHAR( "requestTime", '${units[unit]}' ) as time`)
    ]).where("requestTime", '>=', moment(startTime).format('YYYY-MM-DD HH:mm:ss'))
      .where("requestTime", '<', moment(endTime).format('YYYY-MM-DD HH:mm:ss'))
      .groupBy('time');

    if (botId) {
      query.where("botId", botId)
    }
    return await query;
  }

}

export default QueueLogModel
