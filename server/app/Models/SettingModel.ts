import BaseModel from './BaseModel'

class SettingModel extends BaseModel {
  static tableName = "settings"

  //fields
  id: number;
  name: string;
  description: string;
  key: string;
  value: string;

  static async getSettingByKey(key: string) {
    return this.query().where('key', key).first()
  }
}

export default SettingModel
