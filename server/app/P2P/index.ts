function parseJson(data) {
  let result;
  try {
    result = JSON.parse(data);
  } catch (err) {
    console.log(chalk.yellow("cant parse"));
    result = data;
  }
  return result;
}
class P2PService {
  private static execPath = path.resolve(
    "server/app/Bin/mq-p2p-client-linux-x64"
  );

  static async getConfig() {
    let cmd = await $`${this.execPath} get-config`;
    return parseJson(cmd.stdout);
  }

  static async stop() {
    let cmd = await $`${this.execPath} stop`;
    return parseJson(cmd.stdout);
  }

  static async startup() {
    let cmd = await $`sudo ${this.execPath} startup`;
    return parseJson(cmd.stdout);
  }

  // static async getPeerId(serialNumber) {
  //   return REGISTERED_DEVICES[serialNumber] || "box-6.76";
  // }

  // static async getCamera(peerId, cameraId) {
  //   return CAMERA_MOCKS[peerId] ? CAMERA_MOCKS[peerId][cameraId] : [];
  // }

  // static async getCameras(peerId) {
  //   return CAMERA_MOCKS[peerId] || [];
  // }

  static async getProtocol(peerId, camera) {
    let peerInfo = await P2PService.getConfig();
    let findProtocol = (peerInfo.protocols || []).find(
      (val) => val.protocol === camera.protocol && peerId === val.peerId
    );
    return {
      // host: findProtocol.host,
      port: Number(findProtocol.port),
      host: "127.0.0.1",
    };
  }
}

export default P2PService;
