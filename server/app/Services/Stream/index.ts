import { ChildProcessWithoutNullStreams, spawn } from "child_process";
import ffmpegStatic from "ffmpeg-static";
import fs from "fs";
import path from "path";
import StreamSession from "./session";

// export interface StreamData {
//   key: string;
//   url: string;
//   output: string;
//   transcoder: ChildProcessWithoutNullStreams;
//   status: string;
// }

export default class StreamService {
  static sessions = new Map();

  static async getHlsStream(key: string, inputs: string[], output: string) {
    if (this.sessions.has(key)) {
      return this.sessions.get(key);
    }

    let streamDir = path.join(output, key);
    if (fs.existsSync(streamDir)) {
      fs.rmdirSync(streamDir, { recursive: true });
    }
    fs.mkdirSync(streamDir, { recursive: true });

    const args = this.makeArgs(inputs, streamDir);

    return await new Promise((resolve, reject) => {
      const stream = new StreamSession(key, args);
      this.sessions.set(key, stream);

      const timeCheck = setTimeout(() => {
        stream.stop();
        reject("timeout");
      }, 15 * 1000);

      stream.on("error", ({ id, err }) => {
        console.log("sessionError=" + id, err);
        this.sessions.delete(id);
        reject(err);
      });
      stream.on("end", ({ id, code }) => {
        console.log("sessionEnd=" + id, code);
        // this.sessions.delete(id);
      });
      stream.on("ready", ({ id }) => {
        clearTimeout(timeCheck);
        resolve(id);
      });
    });
  }

  static makeArgs(inputs, outDir) {
    const args = [];
    inputs.forEach((input) => {
      args.push("-rtsp_transport", "tcp");
      args.push("-thread_queue_size", "1024");
      args.push("-i", input);
    });

    if (inputs.length > 1) {
      let filters = "";
      for (let i = 0; i < inputs.length; i++) {
        filters += `[${i}:v]`;
      }
      filters += `concat=n=${inputs.length}:v=1[v]`;
      args.push("-filter_complex", filters);
      args.push("-map", "[v]");
    }

    args.push("-preset", "veryfast");
    // args.push("-hls_flags", "split_by_time");
    args.push("-f", "hls");
    args.push("-hls_time", 4);
    // args.push("-hls_playlist_type", "event");
    args.push("-hls_flags", "split_by_time");
    args.push("-hls_list_size", "0");

    args.push(outDir + "/index.m3u8");

    return args;
  }
}
