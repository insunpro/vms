import { spawn } from "child_process";
import { EventEmitter } from "stream";
import ffmpeg from "ffmpeg-static";

export default class StreamSession extends EventEmitter {
  session: string;
  inputs: string[];
  args: string[];
  ffmpeg;

  constructor(key, args) {
    super();
    this.session = key;
    this.args = args;
    this.start();
  }

  async start() {
    this.ffmpeg = spawn(ffmpeg, this.args);

    this.ffmpeg.stdout.on("data", (chunk) =>
      console.log(`sid=${this.session}`, chunk.toString())
    );
    this.ffmpeg.stderr.on("data", (chunk) => {
      if (chunk.toString().indexOf("index0.ts") > -1) {
        this.emit("ready", { id: this.session });
      }
      console.log(`sid=${this.session}`, chunk.toString());
    });

    this.ffmpeg.on("close", (code, signal) => {
      this.emit("end", { id: this.session, code });
    });
    this.ffmpeg.on("error", (err) => {
      this.emit("error", { id: this.session, err });
    });
  }

  stop() {
    this.ffmpeg.kill();
  }
}
