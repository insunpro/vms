export function generateNewSessionID(pool) {
  let sessionID = "";
  const possible = "ABCDEFGHIJKLMNOPQRSTUVWKYZ0123456789";
  const numPossible = possible.length;
  do {
    for (let i = 0; i < 8; i++) {
      sessionID += possible.charAt((Math.random() * numPossible) | 0);
    }
  } while (pool.has(sessionID));
  return sessionID;
}
