import superagent from "superagent";
const WEBRTC_URL = process.env.WEBRTC_URL || "http://127.0.0.1:8083/stream";

export default class WebRtcService {
  static async getRemoteSdp(host: string, channel: string, sdp: any) {
    let suuid = channel === "main" ? host : host + "_" + channel;
    let url = WEBRTC_URL + "/receiver/" + suuid;

    return new Promise((resolve, reject) => {
      superagent
        .post(url)
        .type("form")
        .send({
          suuid,
          data: sdp,
        })
        .end((err, res) => {
          if (err) return reject(err);
          resolve(res.text);
        });
    });
  }

  static async getCodecInfo(host: string, channel: string) {
    let suuid = channel === "main" ? host : host + "_" + channel;
    let url = WEBRTC_URL + "/codec/" + suuid;

    return new Promise((resolve, reject) => {
      superagent.get(url, (err, res) => {
        if (err) return reject(err);
        let data;
        try {
          data = JSON.parse(res.text || "[]");
        } catch (error) {
          return reject(error);
        } finally {
          resolve(data);
        }
      });
    });
  }
}
