if (!process.env.IS_TS_NODE) {
  // tslint:disable-next-line:no-var-requires
  require('module-alias/register');
}
import Server from '@core/Server'

(async () => {
  try {
    let server = new Server();
    await server.start()

    await import("zx/globals");
    $.verbose = false;

    //WhatsApp.init();
    //const DncBot = require('@app/Services/DNC').default
    //DncBot.init()
    //const PDPC = require('@app/Services/PDPC').default
    //PDPC.init();
    //require('@app/Cronjob')
  } catch (e) {
    console.error(e);
    process.exit(1);
  }
})();
