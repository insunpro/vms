import nextI18NextConfig from "@root/next-i18next.config";
const Cookies = require("universal-cookie");
import nextCookie from "next-cookies";

export const getLocale = (request) => {
  const defaultLocale = getDefaultLocale();
  const cookie = nextCookie({
    req: request,
  });
  const cookies = new Cookies(cookie);
  return cookies.get("NEXT_LOCALE") || defaultLocale;
};

export const getDefaultLocale = () => {
  const defaultLocale = nextI18NextConfig.i18n.defaultLocale;
  return defaultLocale;
};
