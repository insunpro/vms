require("dotenv").config();
import next from "next";
import express, { Response } from "express";
import Router from "./Routes";
import Database from "@core/Databases";

let bodyParser = require("body-parser");
const cors = require("cors");
const { parse } = require('url')

//const nextI18NextMiddleware = require('next-i18next/middleware').default
//const middleware = require('i18next-http-middleware')
//const nextI18next = require('@libs/I18n').default
//import { serverSideTranslations } from 'next-i18next/serverSideTranslations';

const dev = process.env.NODE_ENV !== "production";
const nextApp = next({ dev });
let handle: Function = nextApp.getRequestHandler();

class Server {
  express = express();
  host;
  port;
  options: any;
  static nextApp: any;

  constructor({
    host,
    port,
    options = {},
  }: { host?: string; port?: string | number; options?: any } = {}) {
    const defaultPORT =
      process.env.MODE == "dev-client"
        ? process.env.DEV_FRONTEND_PORT
        : process.env.PORT;
    this.host = host || process.env.HOST || "0.0.0.0";
    this.port = Number(port) || Number(defaultPORT) || 3333;
    this.options = options;
  }

  async connectDatabase() {
    if (process.env.MODE != "dev-client") {
      return await Database.connect();
    }
  }

  async nextStart() {
    if (process.env.MODE !== "dev-server") {
      await nextApp.prepare();
      Server.nextApp = nextApp;
    } else {
      handle = (request: Request, response: Response) => {
        response.status(404).send("MODE is dev server only, Route not exist.");
      };
      Server.nextApp = { render: handle };
    }
  }

  async start() {
    await this.nextStart();
    await this.connectDatabase();

    //await nextI18next.initPromise
    //await serverSideTranslations(locale, ['common', 'footer'])
    //this.express.use(middleware(nextI18next))

    this.express.use(bodyParser.urlencoded({ extended: true, limit: "500mb" }));
    this.express.use(
      bodyParser.json({
        limit: "500mb",
      })
    );
    this.express.use(
      cors({
        origin: dev ? "*" : process.env.CORS_ORIGIN,
      })
    );

    this.express.use(Router.build());

    this.express.all("*", (req, res) => {
     // console.log(req);
      res.setHeader("Cache-Control", "public, max-age=31536000, immutable");
      const parsedUrl = parse(req.url, true);
     // const { pathname, query } = parsedUrl;
      return handle(req, res, parsedUrl);
    });

    await new Promise<void>((r) =>
      this.express.listen(this.port, this.host, () => {
        console.log(`server stated: ${this.host}:${this.port}`);
        r();
      })
    );
    return {
      express: this.express,
      next: Server.nextApp,
    };
  }
}

export default Server;
