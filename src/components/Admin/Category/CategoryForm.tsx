import React from "react";
import { Form, Input, Select } from "antd";
import useBaseHook from "@src/hooks/BaseHook";
import categoryService from "@src/services/categoryService";
import useSWR from "swr";

const { Option } = Select;

const CategoryForm = () => {
  const { t, getData, router } = useBaseHook();
  const { query } = router;
  const { data: dataT } = useSWR("selectParent", () =>
    categoryService().withAuth().index({ pageSize: -1 })
  );
  const categories = getData(dataT, "data", []);

  return (
    <>
      <Form.Item
        label={t("pages:categories.table.name")}
        name="name"
        rules={[
          {
            required: true,
            message: t("messages:form.required", {
              name: t("pages:categories.table.name"),
            }),
          },
          {
            whitespace: true,
            message: t("messages:form.required", {
              name: t("pages:categories.table.name"),
            }),
          },
          {
            max: 255,
            message: t("messages:form.maxLength", {
              name: t("pages:categories.table.name"),
              length: 255,
            }),
          },
        ]}
      >
        <Input placeholder={t("pages:categories.table.name")} />
      </Form.Item>
      <Form.Item
        label={t("pages:categories.table.description")}
        name="description"
        rules={[
          {
            max: 255,
            message: t("messages:form.maxLength", {
              name: t("pages:categories.table.description"),
              length: 255,
            }),
          },
        ]}
      >
        <Input placeholder={t("pages:categories.table.description")} />
      </Form.Item>
      <Form.Item
        label={t("pages:categories.form.parent")}
        name="parentId"
        rules={
          [
            //  { required: true, message: t('messages:form.required', {name: t('pages:categories.form.parent')}) },
          ]
        }
      >
        <Select
          placeholder={t("pages:categories.form.parent")}
          allowClear
          showSearch
        >
          {categories
            .filter((category) => category.id != query.id)
            .map((item: any) => (
              <Option value={item.id} key={item.name || ""}>
                {item.name}
              </Option>
            ))}
        </Select>
      </Form.Item>
    </>
  );
};

export default CategoryForm;
