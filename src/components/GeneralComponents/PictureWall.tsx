import { Button, Image, Modal } from "antd";
import { useState } from "react";

const PictureWall = ({ urls, maxDisplay = 5 }) => {
  const [visible, setVisible] = useState(false);
  const [preview, setPreview] = useState(null);

  let _maxDisplay = urls.length >= maxDisplay ? maxDisplay : urls.length;

  return (
    <div className="picture-wall">
      <div>
        {urls.map((url, index) => {
          if (index <= _maxDisplay) {
            return (
              <div className="picture-wall-placeholder" key={index}>
                <Image
                  key={index}
                  src={url}
                  height="100%"
                  preview={false}
                  onClick={() => {
                    setPreview(index);
                    setVisible(true);
                  }}
                />
              </div>
            );
          }
        })}

        {urls.length > _maxDisplay && (
          <span onClick={() => setVisible(true)}>
            <a onClick={(e) => e.preventDefault()}>
              +{urls.length - _maxDisplay} ảnh khác
            </a>
          </span>
        )}
      </div>
      <Modal
        visible={visible}
        onCancel={() => setVisible(false)}
        maskClosable={false}
        footer={""}
      >
        <div className="picture-wall-preview">
          <Image src={urls[preview]} height="100%" preview={false} />
        </div>

        <div className="picture-wall-all">
          {urls.map((url, index) => {
            return (
              <div
                key={index}
                className={`picture-wall-all-image ${
                  index === preview ? "active" : ""
                }`}
              >
                <Image
                  src={url}
                  height="100%"
                  preview={false}
                  onClick={() => setPreview(index)}
                />
              </div>
            );
          })}
        </div>
      </Modal>
    </div>
  );
};

export default PictureWall;
