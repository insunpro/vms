import useBaseHooks from "@root/src/hooks/BaseHook";
import { Checkbox, Menu } from "antd";
import _ from "lodash";
import { useEffect } from "react";
// import SelectColor from "./selectColor";
import SelectGender from "../newFilters/selectGender";
import SelectAge from "../newFilters/selectAge";
import SelectGlasses from "../newFilters/selectGlasses";
import SelectMask from "../newFilters/selectMask";
import SelectBeard from "../newFilters/selectBeard";
import SelectExpression from "../newFilters/selectExpression";
import { defaultFaceFilter } from "@root/config/constant";
// import SelectOutfit from "./selectOutfit";

const FaceFilter = () => {
  const { getStore, setStore } = useBaseHooks();

  useEffect(() => {
    setStore("faceFilters", defaultFaceFilter);
    return () => {
      setStore("faceFilters", defaultFaceFilter);
    };
  }, []);

  const filters = getStore("faceFilters") || {};

  const onFilterChange = (key, value) => {
    setStore(`faceFilters.${key}`, value);
  };

  return (
    <>
      <Menu
        theme="dark"
        style={{ width: 220 }}
        mode="inline"
        defaultOpenKeys={[]}
        className="filter-menu"
      >
        <Menu.SubMenu title="Giới tính" key="gender">
          <Menu.ItemGroup className="mg-8">
            <SelectGender
              onChange={(values) => onFilterChange("gender", values)}
              target="face"
            />
          </Menu.ItemGroup>
        </Menu.SubMenu>
        <Menu.SubMenu title="Độ tuổi" key="age">
          <Menu.ItemGroup className="mg-8">
            <SelectAge
              onChange={(values) => onFilterChange("gender", values)}
              target="face"
            />
          </Menu.ItemGroup>
        </Menu.SubMenu>
        <Menu.SubMenu title="Đeo kính" key="glasses">
          <Menu.ItemGroup className="mg-8">
            <SelectGlasses
              onChange={(values) => onFilterChange("gender", values)}
            />
          </Menu.ItemGroup>
        </Menu.SubMenu>
        <Menu.SubMenu title="Khẩu trang" key="faceMask">
          <Menu.ItemGroup className="mg-8">
            <SelectMask
              onChange={(values) => onFilterChange("gender", values)}
            />
          </Menu.ItemGroup>
        </Menu.SubMenu>
        <Menu.SubMenu title="Râu" key="beard">
          <Menu.ItemGroup className="mg-8">
            <SelectBeard
              onChange={(values) => onFilterChange("gender", values)}
            />
          </Menu.ItemGroup>
        </Menu.SubMenu>
        <Menu.SubMenu title="Biểu cảm" key="expression">
          <Menu.ItemGroup className="mg-8">
            <SelectExpression
              onChange={(values) => onFilterChange("gender", values)}
            />
          </Menu.ItemGroup>
        </Menu.SubMenu>
      </Menu>
    </>
  );
};

export default FaceFilter;
