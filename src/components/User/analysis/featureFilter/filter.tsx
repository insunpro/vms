import { defaultFeatureFilter } from "@root/config/constant";
import useBaseHooks from "@root/src/hooks/BaseHook";
import { Checkbox, Menu } from "antd";
import _ from "lodash";
import { useEffect } from "react";
import SelectAge from "../newFilters/selectAge";
import SelectBag from "../newFilters/selectBag";
import SelectBot from "../newFilters/selectBot";
// import SelectColor from "./selectColor";
import SelectGender from "../newFilters/selectGender";
import SelectTop from "../newFilters/selectTop";
// import SelectOutfit from "./selectOutfit";
// import UploadImage from "./uploadImage";

const FeatureFilter = () => {
  const { getStore, setStore } = useBaseHooks();

  useEffect(() => {
    setStore("featureFilters", defaultFeatureFilter);
    return () => {
      setStore("featureFilters", defaultFeatureFilter);
    };
  }, []);

  const filters = getStore("filters");

  const onFilterChange = (key, value) => {
    setStore(`featureFilters.${key}`, value);
  };

  return (
    <>
      <Menu
        theme="dark"
        style={{ width: 220 }}
        mode="inline"
        defaultOpenKeys={[]}
        className="filter-menu"
      >
        <Menu.SubMenu title="Giới tính" key="gender">
          <Menu.ItemGroup className="mg-8">
            <SelectGender
              onChange={(values) => onFilterChange("gender", values)}
              target="feature"
            />
          </Menu.ItemGroup>
        </Menu.SubMenu>
        <Menu.SubMenu title="Độ tuổi" key="age">
          <Menu.ItemGroup className="mg-8">
            <SelectAge
              onChange={(values) => onFilterChange("age", values)}
              target="feature"
            />
          </Menu.ItemGroup>
        </Menu.SubMenu>
        <Menu.SubMenu title="Áo" key="top">
          <Menu.ItemGroup className="mg-8">
            <SelectTop
              onChange={(values) => onFilterChange("gender", values)}
            />
          </Menu.ItemGroup>
        </Menu.SubMenu>
        <Menu.SubMenu title="Quần" key="bottom">
          <Menu.ItemGroup className="mg-8">
            <SelectBot
              onChange={(values) => onFilterChange("gender", values)}
            />
          </Menu.ItemGroup>
        </Menu.SubMenu>
        <Menu.SubMenu title="Phụ kiện" key="bag">
          <Menu.ItemGroup className="mg-8">
            <SelectBag
              onChange={(values) => onFilterChange("gender", values)}
            />
          </Menu.ItemGroup>
        </Menu.SubMenu>
      </Menu>
    </>
  );
};

export default FeatureFilter;
