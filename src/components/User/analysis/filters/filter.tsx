import useBaseHooks from "@root/src/hooks/BaseHook";
import { Checkbox, Menu } from "antd";
import _ from "lodash";
import { useEffect } from "react";
import SelectColor from "./selectColor";
import SelectFigure from "./selectFigure";
import SelectGender from "./selectGender";
import SelectOutfit from "./selectOutfit";

const AnalysisFilter = () => {
  const { getStore, setStore } = useBaseHooks();

  useEffect(() => {
    setStore("filters", {});
    return () => {
      setStore("filters", {});
    };
  }, []);

  const filters = getStore("filters");

  const onFilterChange = (key, values) => {
    setStore("steps", 1);
    if (!values || !values.length) {
      setStore(`filters.${key}`, values);
      return;
    }
    setStore(`filters.${key}`, values);
  };

  return (
    <>
      <Menu
        theme="dark"
        style={{ width: 220 }}
        mode="inline"
        defaultOpenKeys={["gender", "color", "feature", "color", "outfit"]}
        className="filter-menu"
      >
        <Menu.SubMenu title="Giới tính" key="gender">
          <Menu.ItemGroup className="mg-8">
            <SelectGender
              onChange={(values) => onFilterChange("gender", values)}
            />
          </Menu.ItemGroup>
        </Menu.SubMenu>
        <Menu.SubMenu title="Ngoại hình" key="feature">
          <Menu.ItemGroup className="mg-8">
            <SelectFigure
              onChange={(values) => onFilterChange("figure", values)}
            />
          </Menu.ItemGroup>
        </Menu.SubMenu>
        <Menu.SubMenu title="Màu sắc" key="color">
          <Menu.ItemGroup className="mg-8">
            <SelectColor
              onChange={(values) => onFilterChange("color", values)}
            />
          </Menu.ItemGroup>
        </Menu.SubMenu>
        <Menu.SubMenu title="Trang phục" key="outfit">
          <Menu.ItemGroup className="mg-8">
            <SelectOutfit
              onChange={(values) => onFilterChange("outfit", values)}
            />
          </Menu.ItemGroup>
        </Menu.SubMenu>
      </Menu>
    </>
  );
};

export default AnalysisFilter;
