import { Checkbox, Col, Row } from "antd";
import { useEffect, useState } from "react";
import _ from "lodash";
import useBaseHooks from "@root/src/hooks/BaseHook";
import { colorOptions } from "@root/config/constant";

export interface SelectColorProps {
  onChange?: (values) => any;
  all?: boolean;
}
const SelectColor = ({ onChange, all }: SelectColorProps) => {
  const { setStore, getStore } = useBaseHooks();
  const stateValues = getStore("filters.color") || [];

  const step = getStore("steps") || 0;

  const selectOne = (val, checked) => {
    let selected = [...stateValues];
    if (!checked) _.pull(selected, val);
    else selected.push(val);
    onChange && onChange(selected);
  };

  const selectAll = (checked) => {
    let selected = [];
    if (checked) {
      selected = [..._.map(colorOptions, "value"), "all"];
    }
    onChange && onChange(selected);
  };
  return (
    <Checkbox.Group value={stateValues} disabled={step < 1}>
      <Row>
        {all && (
          <Col span={24} key="all">
            <Checkbox
              value={"all"}
              onChange={(e) => selectAll(e.target.checked)}
              key="all"
              indeterminate={
                stateValues.length > 0 &&
                stateValues.length <= colorOptions.length
              }
            >
              Chọn tất cả
            </Checkbox>
          </Col>
        )}
        {colorOptions.map((option) => {
          return (
            <Col span={12} key={option.value}>
              <Checkbox
                value={option.value}
                key={option.value}
                onChange={(e) => selectOne(option.value, e.target.checked)}
              >
                {option.label}
              </Checkbox>
            </Col>
          );
        })}
      </Row>
    </Checkbox.Group>
  );
};

export default SelectColor;
