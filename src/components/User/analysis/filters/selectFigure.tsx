import { Button, Checkbox, Col, Divider, Row } from "antd";
import { useEffect, useState } from "react";
import _ from "lodash";
import useBaseHooks from "@root/src/hooks/BaseHook";
import { figureOptions } from "@root/config/constant";

export interface SelectFigureProps {
  onChange?: (values) => any;
  all?: boolean;
}
const SelectFigure = ({ onChange, all }: SelectFigureProps) => {
  const { setStore, getStore } = useBaseHooks();

  const stateValues = getStore("filters.figure") || [];
  const step = getStore("steps") || 0;

  const selectOne = (val, checked) => {
    let selected = [...stateValues];
    if (!checked) _.pull(selected, val);
    else selected.push(val);
    onChange && onChange(selected);
  };

  const selectAll = (checked) => {
    let selected = [];
    if (checked) {
      selected = [..._.map(figureOptions, "value")];
    }
    onChange && onChange(selected);
  };
  return (
    <div className="child-select">
      <Divider plain dashed>
        <Button
          size="small"
          ghost={stateValues.length < figureOptions.length ? true : false}
          type="primary"
          onClick={() =>
            stateValues.length < figureOptions.length
              ? selectAll(true)
              : selectAll(false)
          }
        >
          Kiểu tóc
        </Button>
      </Divider>
      <Checkbox.Group
        value={stateValues}
        style={{ width: "100%" }}
        disabled={step < 1}
      >
        <Row>
          {figureOptions.map((option) => {
            return (
              <Col span={12} key={option.value}>
                <Checkbox
                  value={option.value}
                  key={option.value}
                  onChange={(e) => selectOne(option.value, e.target.checked)}
                >
                  {option.label}
                </Checkbox>
              </Col>
            );
          })}
        </Row>
      </Checkbox.Group>
    </div>
  );
};

export default SelectFigure;
