import { Checkbox, Col, Row } from "antd";
import { useEffect, useState } from "react";
import _ from "lodash";
import useBaseHooks from "@root/src/hooks/BaseHook";
import { genderOptions } from "@root/config/constant";

export interface SelectGenderProps {
  onChange?: (values) => any;
  all?: boolean;
}
const SelectGender = ({ onChange, all }: SelectGenderProps) => {
  // const [values, setValues] = useState([]);
  const { setStore, getStore } = useBaseHooks();
  const stateValues = getStore("filters.gender") || [];
  const step = getStore("steps") || 0;

  // useEffect(() => {
  //   setValues(stateValues || []);
  // }, [stateValues]);

  const selectOne = (val, checked) => {
    // setValues((prevStates) => {
    let selected = [...stateValues];
    if (!checked) _.pull(selected, val);
    else selected.push(val);
    onChange && onChange(selected);
    // setStore("filters.gender", selected);
    // return selected;
    // });
  };

  const selectAll = (checked) => {
    let selected = [];
    if (checked) {
      selected = [..._.map(genderOptions, "value"), "all"];
    }
    // setValues(selected);
    onChange && onChange(selected);
  };
  return (
    <Checkbox.Group
      value={stateValues}
      style={{ width: "100%" }}
      disabled={step < 1}
    >
      <Row>
        {all && (
          <Col span={24} key="all">
            <Checkbox
              value={"all"}
              onChange={(e) => selectAll(e.target.checked)}
              key="all"
              indeterminate={
                stateValues.length > 0 &&
                stateValues.length <= genderOptions.length
              }
            >
              Chọn tất cả
            </Checkbox>
          </Col>
        )}
        {genderOptions.map((option) => {
          return (
            <Col span={12} key={option.value}>
              <Checkbox
                value={option.value}
                key={option.value}
                onChange={(e) => selectOne(option.value, e.target.checked)}
              >
                {option.label}
              </Checkbox>
            </Col>
          );
        })}
      </Row>
    </Checkbox.Group>
  );
};

export default SelectGender;
