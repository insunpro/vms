import { Checkbox, Col, Row } from "antd";
import { useEffect, useState } from "react";
import _ from "lodash";
import useBaseHooks from "@root/src/hooks/BaseHook";
import { outfitOptions } from "@root/config/constant";

export interface SelectOutfitProps {
  onChange?: (values) => any;
  all?: boolean;
}
const SelectOutfit = ({ onChange, all }: SelectOutfitProps) => {
  const { setStore, getStore } = useBaseHooks();
  const stateValues = getStore("filters.outfit") || [];
  const step = getStore("steps") || 0;

  const selectOne = (val, checked) => {
    let selected = [...stateValues];
    if (!checked) _.pull(selected, val);
    else selected.push(val);
    onChange && onChange(selected);
  };

  const selectAll = (checked) => {
    let selected = [];
    if (checked) {
      selected = [..._.map(outfitOptions, "value"), "all"];
    }
    onChange && onChange(selected);
  };
  return (
    <Checkbox.Group
      value={stateValues}
      style={{ width: "100%" }}
      disabled={step < 1}
    >
      <Row>
        {all && (
          <Col span={24} key="all">
            <Checkbox
              value={"all"}
              onChange={(e) => selectAll(e.target.checked)}
              key="all"
              indeterminate={
                stateValues.length > 0 &&
                stateValues.length <= outfitOptions.length
              }
            >
              Chọn tất cả
            </Checkbox>
          </Col>
        )}
        {outfitOptions.map((option) => {
          return (
            <Col span={12} key={option.value}>
              <Checkbox
                value={option.value}
                key={option.value}
                onChange={(e) => selectOne(option.value, e.target.checked)}
              >
                {option.label}
              </Checkbox>
            </Col>
          );
        })}
      </Row>
    </Checkbox.Group>
  );
};

export default SelectOutfit;
