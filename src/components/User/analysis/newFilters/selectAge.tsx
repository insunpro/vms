import { Checkbox, Col, Row, Select } from "antd";
import { useEffect, useState } from "react";
import _ from "lodash";
import useBaseHooks from "@root/src/hooks/BaseHook";
import { faceAgeOptions, featureAgeOptions } from "@root/config/constant";

export interface SelectAgeProps {
  onChange?: (values) => any;
  all?: boolean;
  target: "face" | "feature";
}
const SelectAge = ({ onChange, all, target }: SelectAgeProps) => {
  const { setStore, getStore } = useBaseHooks();
  const stateValue = getStore(`${target}Filters.age`) || "all";

  return (
    <Select
      style={{ width: "100%" }}
      value={stateValue}
      onChange={(val) => setStore(`${target}Filters.age`, val)}
    >
      <Select.Option value="all" key="all">
        Tất cả
      </Select.Option>
      {target === "feature"
        ? featureAgeOptions.map((option) => {
            return (
              <Select.Option
                value={option.value}
                key={option.value}
                // onChange={(e) => selectOne(option.value, e.target.checked)}
              >
                {option.label}
              </Select.Option>
            );
          })
        : faceAgeOptions.map((option) => {
            return (
              <Select.Option
                value={option.value}
                key={option.value}
                // onChange={(e) => selectOne(option.value, e.target.checked)}
              >
                {option.label}
              </Select.Option>
            );
          })}
    </Select>
  );
};

export default SelectAge;
