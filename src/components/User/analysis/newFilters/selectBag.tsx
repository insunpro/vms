import { Checkbox, Col, Row, Select } from "antd";
import { useEffect, useState } from "react";
import _ from "lodash";
import useBaseHooks from "@root/src/hooks/BaseHook";
import { bagOptions } from "@root/config/constant";

export interface SelectBagProps {
  onChange?: (values) => any;
  all?: boolean;
}
const SelectBag = ({ onChange, all }: SelectBagProps) => {
  const { setStore, getStore } = useBaseHooks();
  const bag = getStore("featureFilters.bag") || "all";

  return (
    <Select
      style={{ width: "100%" }}
      value={bag}
      onChange={(val) => setStore("featureFilters.bag", val)}
    >
      <Select.Option value="all" key="all">
        Tất cả
      </Select.Option>
      {bagOptions.map((option) => {
        return (
          <Select.Option
            value={option.value}
            key={option.value}
            // onChange={(e) => selectOne(option.value, e.target.checked)}
          >
            {option.label}
          </Select.Option>
        );
      })}
    </Select>
  );
};

export default SelectBag;
