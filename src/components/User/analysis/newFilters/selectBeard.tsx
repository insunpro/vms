import { Checkbox, Col, Row, Select } from "antd";
import { useEffect, useState } from "react";
import _ from "lodash";
import useBaseHooks from "@root/src/hooks/BaseHook";
import { beardOptions } from "@root/config/constant";

export interface SelectBeardProps {
  onChange?: (values) => any;
  all?: boolean;
}
const SelectBeard = ({ onChange, all }: SelectBeardProps) => {
  const { setStore, getStore } = useBaseHooks();
  const stateValue = getStore("faceFilters.beard") || "all";

  return (
    <Select
      style={{ width: "100%" }}
      value={stateValue}
      onChange={(val) => setStore("faceFilters.beard", val)}
    >
      <Select.Option value="all" key="all">
        Tất cả
      </Select.Option>
      {beardOptions.map((option) => {
        return (
          <Select.Option
            value={option.value}
            key={option.value}
            // onChange={(e) => selectOne(option.value, e.target.checked)}
          >
            {option.label}
          </Select.Option>
        );
      })}
    </Select>
  );
};

export default SelectBeard;
