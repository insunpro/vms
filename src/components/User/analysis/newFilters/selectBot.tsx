import { Button, Checkbox, Col, Divider, Row, Select } from "antd";
import { useEffect, useState } from "react";
import _ from "lodash";
import useBaseHooks from "@root/src/hooks/BaseHook";
import { botOptions, colorOptions } from "@root/config/constant";

export interface SelectBotProps {
  onChange?: (values) => any;
  all?: boolean;
}
const SelectBot = ({ onChange, all }: SelectBotProps) => {
  const { setStore, getStore } = useBaseHooks();
  const botType = getStore("featureFilters.botType") || "all";
  const botColor = getStore("featureFilters.botColor") || "all";

  return (
    <div>
      <Select
        style={{ width: "100%" }}
        value={botType}
        onChange={(val) => setStore("featureFilters.botType", val)}
      >
        <Select.Option value="all" key="all">
          Tất cả
        </Select.Option>
        {botOptions.map((option) => {
          return (
            <Select.Option
              value={option.value}
              key={option.value}
              // onChange={(e) => selectOne(option.value, e.target.checked)}
            >
              {option.label}
            </Select.Option>
          );
        })}
      </Select>
      <Divider plain dashed>
        <Button size="small" ghost type="primary">
          Màu sắc
        </Button>
      </Divider>
      <Select
        style={{ width: "100%" }}
        value={botColor}
        onChange={(val) => setStore("featureFilters.botColor", val)}
      >
        <Select.Option value="all" key="all">
          <span>Tất cả</span>
          <span className="color color-full select-color"></span>
        </Select.Option>
        {colorOptions.map((option) => {
          return (
            <Select.Option
              value={option.value}
              key={option.value}
              // onChange={(e) => selectOne(option.value, e.target.checked)}
            >
              <span>{option.label}</span>
              <span
                className={`color color-${option.value} select-color`}
              ></span>
            </Select.Option>
          );
        })}
      </Select>
    </div>
  );
};

export default SelectBot;
