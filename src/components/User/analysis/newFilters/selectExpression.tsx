import { Checkbox, Col, Row, Select } from "antd";
import { useEffect, useState } from "react";
import _ from "lodash";
import useBaseHooks from "@root/src/hooks/BaseHook";
import { expressionOptions } from "@root/config/constant";

export interface SelectExpressionProps {
  onChange?: (values) => any;
  all?: boolean;
}
const SelectExpression = ({ onChange, all }: SelectExpressionProps) => {
  const { setStore, getStore } = useBaseHooks();
  const stateValue = getStore("faceFilters.expression") || "all";
  return (
    <Select
      style={{ width: "100%" }}
      value={stateValue}
      onChange={(val) => setStore("faceFilters.expression", val)}
    >
      <Select.Option value="all" key="all">
        Tất cả
      </Select.Option>
      {expressionOptions.map((option) => {
        return (
          <Select.Option
            value={option.value}
            key={option.value}
            // onChange={(e) => selectOne(option.value, e.target.checked)}
          >
            {option.label}
          </Select.Option>
        );
      })}
    </Select>
  );
};

export default SelectExpression;
