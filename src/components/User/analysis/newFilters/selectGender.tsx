import { Checkbox, Col, Row, Select } from "antd";
import { useEffect, useState } from "react";
import _ from "lodash";
import useBaseHooks from "@root/src/hooks/BaseHook";
import { genderOptions } from "@root/config/constant";

export interface SelectGenderProps {
  onChange?: (values) => any;
  all?: boolean;
  target: "face" | "feature";
}
const SelectGender = ({ onChange, all, target }: SelectGenderProps) => {
  const { setStore, getStore } = useBaseHooks();
  const stateValue = getStore(`${target}Filters.gender`) || "all";

  return (
    <Select
      style={{ width: "100%" }}
      value={stateValue}
      onChange={(val) => setStore(`${target}Filters.gender`, val)}
    >
      <Select.Option value="all" key="all">
        Tất cả
      </Select.Option>
      {genderOptions.map((option) => {
        return (
          <Select.Option
            value={option.value}
            key={option.value}
            // onChange={(e) => selectOne(option.value, e.target.checked)}
          >
            {option.label}
          </Select.Option>
        );
      })}
    </Select>
  );
};

export default SelectGender;
