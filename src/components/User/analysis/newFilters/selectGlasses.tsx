import { Checkbox, Col, Row, Select } from "antd";
import { useEffect, useState } from "react";
import _ from "lodash";
import useBaseHooks from "@root/src/hooks/BaseHook";
import { glassesOptions } from "@root/config/constant";

export interface SelectGlassesProps {
  onChange?: (values) => any;
  all?: boolean;
}
const SelectGlasses = ({ onChange, all }: SelectGlassesProps) => {
  const { setStore, getStore } = useBaseHooks();
  const stateValue = getStore("faceFilters.glasses") || "all";

  return (
    <Select
      style={{ width: "100%" }}
      value={stateValue}
      onChange={(val) => setStore("faceFilters.glasses", val)}
    >
      <Select.Option value="all" key="all">
        Tất cả
      </Select.Option>
      {glassesOptions.map((option) => {
        return (
          <Select.Option
            value={option.value}
            key={option.value}
            // onChange={(e) => selectOne(option.value, e.target.checked)}
          >
            {option.label}
          </Select.Option>
        );
      })}
    </Select>
  );
};

export default SelectGlasses;
