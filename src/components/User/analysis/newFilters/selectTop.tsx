import { Button, Checkbox, Col, Divider, Row, Select } from "antd";
import { useEffect, useState } from "react";
import _ from "lodash";
import useBaseHooks from "@root/src/hooks/BaseHook";
import { colorOptions, topOptions } from "@root/config/constant";

export interface SelectTopProps {
  onChange?: (values) => any;
  all?: boolean;
}
const SelectTop = ({ onChange, all }: SelectTopProps) => {
  const { setStore, getStore } = useBaseHooks();
  const topType = getStore("featureFilters.topType") || "all";
  const topColor = getStore("featureFilters.topColor") || "all";

  return (
    <div>
      <Select
        style={{ width: "100%" }}
        value={topType}
        onChange={(val) => setStore("featureFilters.topType", val)}
      >
        <Select.Option value="all" key="all">
          Tất cả
        </Select.Option>
        {topOptions.map((option) => {
          return (
            <Select.Option
              value={option.value}
              key={option.value}
              // onChange={(e) => selectOne(option.value, e.target.checked)}
            >
              {option.label}
            </Select.Option>
          );
        })}
      </Select>
      <Divider plain dashed>
        <Button size="small" ghost type="primary">
          Màu sắc
        </Button>
      </Divider>
      <Select
        style={{ width: "100%" }}
        value={topColor}
        onChange={(val) => setStore("featureFilters.topColor", val)}
      >
        <Select.Option value="all" key="all">
          <span>Tất cả</span>
          <span className="color color-full select-color"></span>
        </Select.Option>
        {colorOptions.map((option) => {
          return (
            <Select.Option
              value={option.value}
              key={option.value}
              // onChange={(e) => selectOne(option.value, e.target.checked)}
            >
              <span>{option.label}</span>
              <span
                className={`color color-${option.value} select-color`}
              ></span>
            </Select.Option>
          );
        })}
      </Select>
    </div>
  );
};

export default SelectTop;
