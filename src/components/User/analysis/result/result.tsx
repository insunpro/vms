import {
  Button,
  Card,
  Col,
  Row,
  Image,
  Tooltip,
  Spin,
  Typography,
  Grid,
} from "antd";
import moment from "moment";
import {
  EyeOutlined,
  ZoomInOutlined,
  PlayCircleOutlined,
} from "@ant-design/icons";
// import devices from "@root/public/static/devices.json";
import ModalPlayback from "../../modal/Playback";
import { useEffect, useRef, useState } from "react";
import { colorOptions, genderOptions } from "@root/config/constant";
import {
  DownOutlined,
  FrownOutlined,
  HomeOutlined,
  VideoCameraOutlined,
  FrownFilled,
} from "@ant-design/icons";
import { useRequest } from "ahooks";
import deviceService from "@root/src/services/deviceService";
import _ from "lodash";
import ResultCard from "./resultCard";

const grids = [
  "",
  "ant-col-24",
  "ant-col-12",
  "ant-col-8",
  "ant-col-6",
  "ant-col-55",
  "ant-col-4",
];

const AnalysisResult = ({
  data = [],
  total,
  onScan,
  type = "feature",
  tab,
  loading = false,
  loadingMore = false,
  onLoadMore = null,
}) => {
  const [visible, setVisible] = useState(false);
  const [preview, setPreview] = useState(null);
  const [_loading, setLoading] = useState(loading);
  const screens = Grid.useBreakpoint();
  const listInnerRef = useRef(null);

  const { data: devices, loading: requestLoading } = useRequest(
    async () => deviceService().withAuth().getDevice({}),
    {
      cacheKey: "devices",
    }
  );

  const channelNames = {};
  (devices || []).map((device) => {
    device.channels.map((channel) => {
      channelNames[Number(channel.logicChannel) + 1] = channel.name;
    });
  });

  const onSearch = (url, type) => {
    onScan && onScan(url, type);
  };

  const onScroll = () => {
    if (listInnerRef.current) {
      const { scrollTop, scrollHeight, clientHeight } = listInnerRef.current;
      if (scrollTop + clientHeight === scrollHeight) {
        onLoadMore && onLoadMore();
      }
    }
  };

  const scrollTop = () => {
    if (listInnerRef.current) {
      //@ts-ignore
      listInnerRef.current.scrollTop = 0;
    }
  };

  useEffect(() => {
    setLoading(loading);
    scrollTop();
  }, [loading]);

  return (
    <Card
      className="card-container "
      title={`Kết quả tìm kiếm ${total < 0 ? "" : `(${total} kết quả)`}`}
      style={{ height: "calc(100vh - 112px)" }}
    >
      <Spin spinning={_loading}>
        <div
          className="analysis-result"
          ref={listInnerRef}
          style={{ height: "calc(100vh - 212px)", overflowY: "scroll" }}
          onScroll={onScroll}
        >
          <Row gutter={[4, 4]}>
            {data.map((record, index) => {
              return (
                <Col className={screens.sm ? grids[4] : grids[5]} key={index}>
                  <ResultCard
                    title={
                      channelNames[Number(record.Channel) + 1] || record.Channel
                    }
                    data={record}
                    onPlayback={(data) => {
                      setVisible(true);
                      setPreview(data);
                    }}
                    onSearch={(img, type) => {
                      onSearch(img, type);
                    }}
                    type={type}
                    tab={tab}
                  />
                </Col>
              );
            })}
            {loadingMore && (
              <Col
                className={screens.sm ? grids[4] : grids[5]}
                key={"loadingMore"}
              >
                <Card
                  loading
                  style={{
                    height: 150,
                    border: "solid 2px green",
                    borderRadius: "0px",
                    backgroundColor: "transparent",
                  }}
                />
              </Col>
            )}
          </Row>
        </div>
      </Spin>
      <ModalPlayback
        visible={visible}
        setVisible={setVisible}
        record={preview}
      />
    </Card>
  );
};

export default AnalysisResult;
