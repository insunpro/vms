import {
  FileSearchOutlined,
  PlayCircleOutlined,
  SmileOutlined,
  SolutionOutlined,
  UserOutlined,
  VideoCameraOutlined,
} from "@ant-design/icons";
import {
  botOptions,
  colorOptions,
  faceAgeOptions,
  genderOptions,
  glassesOptions,
  maskOptions,
  topOptions,
} from "@root/config/constant";
import { Col, Row, Image, Button, Typography, Popover, Tooltip } from "antd";
import _ from "lodash";
import moment from "moment";
import getConfig from "next/config";
import { useState } from "react";

const { publicRuntimeConfig } = getConfig();

const ResultCard = ({
  title,
  data,
  onPlayback = null,
  onSearch = null,
  type = "feature",
  tab,
}) => {
  if (type === "img") {
    if (tab === "face") {
    }
  }
  if (type === "feature") {
    if (tab === "body") {
    }
  }
  const faceImage =
    tab === "face" && type === "feature"
      ? _.get(data, "FilePath", null)
      : _.get(data, "Person.Image[0].FilePath", null);
  const featImage =
    tab === "body" && type === "feature"
      ? _.get(data, "FilePath", null)
      : _.get(data, "Human.Image.FilePath", null);
  const faceSummary = _.get(data, "SummaryNew[0].Value", {});
  const faceAttr = {
    gender: genderOptions.find((val) => val.map === faceSummary["Sex"])?.label,
    age:
      faceAgeOptions.find(
        (val) =>
          val.map[0] < faceSummary["Age"] && val.map[1] > faceSummary["Age"]
      )?.label || faceAgeOptions[5].label,
    glasses: glassesOptions.find((val) => val.map === faceSummary["Glasses"])
      ?.label,
    mask: maskOptions.find((val) => val.map === faceSummary["Mask"])?.label,
  };
  const renderFaceAttr = () => {
    return (
      <div style={{ textAlign: "left" }}>
        <span>Giới tính: {faceAttr.gender || "?"}</span>
        <br />
        <span>Tuổi: {faceAttr.age || "?"}</span>
        <br />
        <span>Kính: {faceAttr.glasses || "?"}</span>
        <br />
        <span>Khẩu trang: {faceAttr.mask || "?"}</span>
      </div>
    );
  };

  const bodySummary = data.Human
    ? _.get(data, "Human", {})
    : _.get(data, "SummaryNew.Value.HumanAttributes", {});

  const bodyAttr = {
    top: topOptions.find((val) => val.map === bodySummary["CoatType"])?.label,
    topColor: colorOptions.find((val) => val.map === bodySummary["CoatColor"])
      ?.value,
    bot: botOptions.find((val) => val.map === bodySummary["TrousersType"])
      ?.label,
    botColor: colorOptions.find(
      (val) => val.map === bodySummary["TrousersColor"]
    )?.value,
  };

  const renderFeatAttr = () => {
    return (
      <div style={{ textAlign: "left" }}>
        <span>Áo: {bodyAttr.top || "?"}</span>
        <br />
        <span>
          Màu áo:{" "}
          <span className={`color color-${bodyAttr.topColor || "full"}`}></span>
        </span>
        <br />
        <span>Quần: {bodyAttr.bot || "?"}</span>
        <br />
        <span>
          Màu Quần:{" "}
          <span className={`color color-${bodyAttr.botColor || "full"}`}></span>
        </span>
      </div>
    );
  };

  return (
    <div className="result-card">
      <div className="result-card-header">
        <div className="result-card-header-title">
          <Typography.Text style={{ maxWidth: 130 }} ellipsis>
            <VideoCameraOutlined />
            {title}
          </Typography.Text>
        </div>
        <div className="result-card-header-extra">
          <Tooltip placement="top" title={"Xem lại"}>
            <span
              className="icon-button"
              onClick={() => onPlayback && onPlayback(data)}
            >
              <PlayCircleOutlined />
            </span>
          </Tooltip>

          {faceImage && (
            <Tooltip placement="top" title={"Truy vết theo khuôn mặt"}>
              <span
                className="icon-button"
                onClick={() => {
                  onSearch &&
                    onSearch(
                      `${publicRuntimeConfig.API_HOST}/api/v1/img?path=` +
                        faceImage,
                      "face"
                    );
                }}
              >
                <UserOutlined />
              </span>
            </Tooltip>
          )}
          {(featImage || (faceImage && tab === "body")) && (
            <Tooltip placement="top" title={"Truy vết ngoại hình"}>
              <span
                className="icon-button"
                onClick={() => {
                  onSearch &&
                    onSearch(
                      `${publicRuntimeConfig.API_HOST}/api/v1/img?path=` +
                        (featImage || faceImage),
                      "body"
                    );
                }}
              >
                <SolutionOutlined />
              </span>
            </Tooltip>
          )}
        </div>
      </div>
      <div className="result-card-body">
        <div className="result-card-body-content">
          <Image
            src={
              `${publicRuntimeConfig.API_HOST}/api/v1/img?path=` +
              (faceImage || featImage)
            }
            height="100%"
            preview={{
              mask: <span>Xem</span>,
            }}
          />
        </div>
        <div className="result-card-body-content">
          {faceImage && featImage && (
            <Image
              src={
                `${publicRuntimeConfig.API_HOST}/api/v1/img?path=` + featImage
              }
              height="100%"
              preview={{
                mask: <span>Xem</span>,
              }}
            />
          )}
          {!featImage && renderFaceAttr()}
          {(!faceImage || featImage) && renderFeatAttr()}
        </div>
      </div>
      <div className="result-card-footer">
        <div className="result-card-footer-time">
          {moment(
            type === "image" ? data.Time : data.StartTime,
            "YYYY-MM-DD HH:mm:ss"
          ).format("HH:mm:ss, DD-MM-YYYY")}
        </div>
        <div className="result-card-footer-extra">
          {data.Similarity && (
            <Tooltip title="Tỷ lệ tương đồng">{data.Similarity}%</Tooltip>
          )}
        </div>
      </div>
    </div>
  );
};

export default ResultCard;
