import useBaseHooks from "@root/src/hooks/BaseHook";
import { Button, Card, DatePicker, Empty, Typography } from "antd";
import { useEffect, useState } from "react";
import ReactPlayer from "react-player";
import VideoPlayer from "../../video/player";

const AnalysisStep1 = ({ next }) => {
  const { getStore, setStore } = useBaseHooks();
  const [verify, setVerify] = useState(false);

  const target = getStore("target");
  const ranges = getStore("ranges");

  const verifyTarget = () => {
    if (!target || !target.type) return setVerify(false);

    if (target.type === "camera" && !ranges) return setVerify(false);

    return setVerify(true);
  };

  useEffect(() => {
    verifyTarget();
  }, [target, ranges]);

  return (
    <div className="step0" style={{ textAlign: "center" }}>
      <div style={{ width: 420, display: "inline-block" }}>
        <Card className="card-transparent">
          {target && target.type === "camera" ? (
            <VideoPlayer url={"/videos/vms-sample-1.mp4"} cam={target.data} />
          ) : target && target.type === "video" ? (
            <ReactPlayer
              url={"/" + target.data.path}
              width="100%"
              height="100%"
              playing={false}
              controls
            />
          ) : (
            <Empty description={"Chọn Camera hoặc Video"} />
          )}
          <br />
          {target && target.type === "camera" && (
            <div
              className="camera-date-picker"
              style={{ width: 360, display: "inline-block" }}
            >
              <Typography.Text>Chọn thời gian</Typography.Text>
              <DatePicker.RangePicker
                style={{ width: "100%" }}
                showTime
                showNow
                onChange={(values) => setStore("ranges", values)}
              />
            </div>
          )}
          {target && target.type === "video" && (
            <div
              className="current-video-name"
              style={{ width: 360, display: "inline-block" }}
            >
              <Typography.Text strong>{target.data.name}</Typography.Text>
            </div>
          )}
        </Card>
        <br />
        <div>
          <Button
            type="primary"
            onClick={() => next()}
            //   loading={loading}
            // ghost
            disabled={!verify}
          >
            Tiếp tục
          </Button>
        </div>
      </div>
    </div>
  );
};

export default AnalysisStep1;
