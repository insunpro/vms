import useBaseHooks from "@root/src/hooks/BaseHook";
import { Button, Card, DatePicker, Empty, Typography } from "antd";
import _ from "lodash";
import { useEffect, useState } from "react";
import ReactPlayer from "react-player";
import VideoPlayer from "../../video/player";

const AnalysisStep2 = ({ back, next, disabled }) => {
  const { getStore, setStore } = useBaseHooks();
  const [active, setActive] = useState(false);

  const filters = getStore("filters") || {};
  const checkFiler = (values) => {
    let notCheck = !values;
    let notCheckAll =
      _.isEmpty(values.gender) &&
      _.isEmpty(values.feature) &&
      _.isEmpty(values.color) &&
      _.isEmpty(values.outfit);
    return notCheck || notCheckAll;
  };

  const renderFilters = () => {
    if (!filters) {
      return <Typography.Text>Chọn thuộc tính hoặc tải ảnh</Typography.Text>;
    }
    let comps = [];
    if (filters && filters.gender && filters.gender.length) {
      comps.push(<div>gender</div>);
    }

    return <div>{comps}</div>;
  };

  return (
    <div className="step1">
      <div style={{ textAlign: "center" }}>
        <div style={{ width: 420, display: "inline-block" }}>
          <Card className="card-transparent">
            <Typography.Text>
              Chọn đặc điểm hoặc tải ảnh nhận diện
            </Typography.Text>
            {/* <UploadImage /> */}
            {/* {renderFilters()} */}
          </Card>
        </div>
      </div>

      <div style={{ textAlign: "center" }}>
        <Button onClick={() => back()} style={{ margin: 8 }}>
          Quay lại
        </Button>
        <Button
          type="primary"
          onClick={() => next()}
          // loading={loading}
          // ghost
          style={{ margin: 8 }}
          disabled={disabled}
        >
          Tiếp tục
        </Button>
      </div>
    </div>
  );
};

export default AnalysisStep2;
