import PictureWall from "@root/src/components/GeneralComponents/PictureWall";
import useBaseHooks from "@root/src/hooks/BaseHook";
import { Button, DatePicker, Empty, Modal, Table, Typography } from "antd";
import ReactPlayer from "react-player";
import VideoPlayer from "../../video/player";
import { PlaySquareOutlined } from "@ant-design/icons";
import { useRef, useState } from "react";
import _ from "lodash";

const AnalysisStep3 = ({ next, data, back }) => {
  const { getStore, setStore } = useBaseHooks();
  const [visible, setVisible] = useState(false);
  const playerRef = useRef(null);
  const target = getStore("target");

  const [playing, setPlaying] = useState(false);

  const columns: any[] = [
    {
      key: "id",
      dataIndex: "id",
      title: "#",
      render: (value, record, index) => index + 1,
    },
    {
      key: "time",
      dataIndex: "time",
      title: "Thời điểm xuất hiện",
    },
    {
      key: "files",
      dataIndex: "files",
      title: "Hình ảnh",
      render: (value, record, index) => {
        let imgs = (value || []).map((file) => {
          return `/${record.filePath}/${file}`;
        });
        return <PictureWall urls={imgs} />;
      },
    },
    {
      key: "task",
      title: "Playback",
      align: "center",
      render: (value, record, index) => {
        return (
          <Button
            icon={<PlaySquareOutlined />}
            onClick={() => seekTo(record.time)}
          >
            Xem lại
          </Button>
        );
      },
    },
  ];

  const seekTo = (seconds) => {
    setVisible(true);
    setTimeout(() => {
      playerRef.current?.seekTo(Math.floor(seconds), "seconds");
    }, 200);
  };
  return (
    <div className="step2">
      <div>
        <Table dataSource={data} columns={columns} size="small" rowKey="id" />
      </div>
      <br />
      <div style={{ textAlign: "center" }}>
        <Button
          // type="primary"
          onClick={() => back()}
          // loading={loading}
          // ghost
          style={{ margin: 8 }}
        >
          Quay lại
        </Button>
        <Button type="primary" onClick={() => next()}>
          Tiếp tục
        </Button>
      </div>
      <Modal
        visible={visible}
        onCancel={() => {
          setVisible(false);
          setPlaying(false);
        }}
        footer={false}
      >
        <div style={{ marginTop: 20 }}>
          <ReactPlayer
            url={`/videos/${_.get(target, "data.name", "video1")}.mp4`}
            width="100%"
            height="100%"
            playing={playing}
            onPlay={() => setPlaying(true)}
            controls
            ref={playerRef}
          />
        </div>
      </Modal>
    </div>
  );
};

export default AnalysisStep3;
