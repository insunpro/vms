import useBaseHooks from "@root/src/hooks/BaseHook";
import { Button, Card, DatePicker, Empty, Typography } from "antd";
import ReactPlayer from "react-player";
import VideoPlayer from "../../video/player";

const AnalysisStep4 = ({ back, next, data }) => {
  const { getStore, setStore } = useBaseHooks();

  return (
    <div className="step4">
      <div style={{ textAlign: "center" }}>
        <div
          style={{
            width: 360,
            height: 240,
            display: "inline-block",
            border: "solid 1px #000",
          }}
        >
          {data ? (
            <ReactPlayer
              url={"/" + data}
              width="100%"
              height="100%"
              playing={false}
              controls
            />
          ) : (
            <Empty />
          )}
        </div>
      </div>

      <div style={{ textAlign: "center" }}>
        <Button
          // type="primary"
          onClick={() => back()}
          // loading={loading}
          // ghost
          style={{ margin: 8 }}
        >
          Quay lại
        </Button>
        <Button
          type="primary"
          onClick={() => next()}
          // loading={loading}
          // ghost
          style={{ margin: 8 }}
        >
          Bắt đầu lại
        </Button>
      </div>
    </div>
  );
};

export default AnalysisStep4;
