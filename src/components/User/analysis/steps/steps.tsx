import useBaseHooks from "@root/src/hooks/BaseHook";
import analysisService from "@root/src/services/analysisService";
import {
  Button,
  Empty,
  Row,
  Spin,
  Tabs,
  Typography,
  Image,
  Collapse,
  Steps,
  TimePicker,
  DatePicker,
  Col,
  Table,
  Card,
} from "antd";
import to from "await-to-js";
import { useState } from "react";
import ReactPlayer from "react-player";
import VideoPlayer from "../../video/player";
import AnalysisStep1 from "./step1";
import AnalysisStep2 from "./step2";
import { PlaySquareOutlined } from "@ant-design/icons";
import PictureWall from "@root/src/components/GeneralComponents/PictureWall";
import AnalysisStep3 from "./step3";
import AnalysisStep4 from "./step4";

const AnalysisSteps = () => {
  const { getStore, setStore } = useBaseHooks();

  const target = getStore("target");
  const filters = getStore("filters");
  const step = getStore("steps") || 0;

  const [loading, setLoading] = useState(false);
  const [data, setData] = useState([]);
  const [video, setVideo] = useState(null);

  const startAnalyzing = async () => {
    setLoading(true);
    let [err, res] = await to(
      analysisService()
        .withAuth()
        .analysis({ ...filters, target })
    );
    setLoading(false);

    setData(res);
    setStore("steps", 2);
  };

  const startSmartVideo = async () => {
    setLoading(true);
    let [err, res] = await to(
      analysisService()
        .withAuth()
        .smartVideo({ ...filters, target })
    );
    setLoading(false);

    setVideo(res);

    setStore("steps", 3);
  };

  return (
    <div
      style={{
        margin: 24,
        borderRadius: 8,
      }}
    >
      <Card title={"Các bước truy vết đối tượng"} className="card-container">
        <Steps current={step} style={{ marginBottom: 24 }}>
          <Steps.Step title="Chọn camera hoặc video" />
          <Steps.Step
            title="Chọn đặc điểm của đối tượng"
            // subTitle="Left 00:00:08"
            // description="This is a description."
          />
          <Steps.Step title="Danh sách đối tượng" />
          <Steps.Step title="Video tóm lược" />
        </Steps>
        {step === 0 && <AnalysisStep1 next={() => setStore("steps", 1)} />}
        {step === 1 && (
          <AnalysisStep2
            next={startAnalyzing}
            back={() => setStore("steps", 0)}
            disabled={false}
          />
        )}
        {step === 2 && (
          <AnalysisStep3
            data={data}
            next={startSmartVideo}
            back={() => setStore("steps", 1)}
          />
        )}
        {step === 3 && (
          <AnalysisStep4
            next={() => setStore("steps", 0)}
            back={() => setStore("steps", 2)}
            data={video}
          />
        )}
      </Card>
    </div>
  );
};

export default AnalysisSteps;
