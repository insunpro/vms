import { Button, Upload } from "antd";
import { useEffect, useState } from "react";
import {
  LoadingOutlined,
  PlusOutlined,
  CloseOutlined,
} from "@ant-design/icons";
import to from "await-to-js";
import analysisService from "@root/src/services/analysisService";
import { colorOptions, outfitOptions } from "@root/config/constant";
import useBaseHooks from "@root/src/hooks/BaseHook";

const UploadImage = ({ onUpload, img, onClear }) => {
  const [loading, setLoading] = useState(false);
  const [preview, setPreview] = useState(null);
  const { setStore } = useBaseHooks();

  const uploadButton = (
    <div>
      {loading ? <LoadingOutlined /> : <PlusOutlined />}
      <div style={{ marginTop: 8 }}>Tải ảnh </div>
    </div>
  );

  const beforeUpload = async (file, fileList) => {
    let reader = new FileReader();
    reader.addEventListener(
      "load",
      () => {
        let data = reader.result;
        setPreview(data);
        onUpload(data);
      },
      false
    );
    reader.readAsDataURL(file);

    setLoading(true);
    // let [err, res] = await to(
    //   analysisService().withAuth().smartPhoto({ image: file })
    // );
    // if (err) {
    //   console.error("uploadImage.err", err);
    // }
    setLoading(false);
    return false;
  };

  const clear = () => {
    setPreview(null);
    onClear && onClear();
  };

  useEffect(() => {
    setPreview(img);
  }, [img]);

  return (
    <div style={{ textAlign: "center" }}>
      <Upload
        name="avatar"
        listType="picture-card"
        className="avatar-uploader"
        showUploadList={false}
        beforeUpload={beforeUpload}
        accept="image/*"
      >
        {preview && !loading ? (
          <img
            src={preview}
            alt="preview"
            style={{ width: "100%", height: "100%" }}
          />
        ) : (
          uploadButton
        )}
      </Upload>
      {preview && (
        <Button type="link" icon={<CloseOutlined />} onClick={clear}>
          Clear
        </Button>
      )}
    </div>
  );
};

export default UploadImage;
