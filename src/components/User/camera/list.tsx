import {
  Avatar,
  Button,
  Card,
  Col,
  Divider,
  Input,
  List,
  Row,
  Spin,
} from "antd";
import Search from "antd/lib/input/Search";
import { useEffect, useState } from "react";
import cameras from "@root/public/static/cameras.json";
import {
  VideoCameraOutlined,
  SearchOutlined,
  SyncOutlined,
  LoadingOutlined,
} from "@ant-design/icons";
import _ from "lodash";
export interface CameraListProps {
  onSelect?: (data) => any;
  reset?: boolean;
}

const CameraList = ({ onSelect, reset }: CameraListProps) => {
  const [data, setData] = useState([]);
  const [selected, setSelected] = useState(null);
  const [searching, setSearching] = useState(false);

  useEffect(() => {
    setTimeout(() => {
      setData(cameras.RECORDS);
    }, 300);
  }, []);

  const handleSearch = (searchValue) => {
    let newData;
    if (!searchValue) {
      newData = cameras.RECORDS;
    } else {
      newData = data.filter(
        (val) =>
          val.name.toLowerCase().indexOf(searchValue.toLowerCase()) !== -1
      );
    }
    setData(newData);
    setSearching(false);
  };

  const debounceSearch = _.debounce(handleSearch, 750);

  useEffect(() => {
    setSelected(null);
  }, [reset]);
  
  return (
    <>
      <div>
        <Input
          placeholder="Tìm kiếm"
          onChange={(e) => {
            setSearching(true);
            debounceSearch(e.target.value);
          }}
          prefix={searching ? <LoadingOutlined /> : <SearchOutlined />}
        />
      </div>
      <br />
      <div className="list-item">
        <List className="input-bg">
          {data.map((item) => {
            return (
              <List.Item
                key={item.id}
                className={`select-list-item ${
                  item.id === selected ? "active" : ""
                }`}
                onClick={() => {
                  setSelected(item.id);
                  onSelect && onSelect(item);
                }}
              >
                <List.Item.Meta
                  avatar={
                    <Avatar
                      icon={<VideoCameraOutlined />}
                      style={{
                        backgroundColor:
                          selected === item.id ? "#f56a00" : "transparent",
                      }}
                    />
                  }
                  title={item.name}
                  description={item.email}
                />
                {/* <div>Content</div> */}
              </List.Item>
            );
          })}
        </List>
      </div>
    </>
  );
};

export default CameraList;
