import { Tree } from "antd";
import { EventDataNode } from "antd/lib/tree";
import { useState } from "react";

// import devices from "@root/public/static/devices.json";
import {
  DownOutlined,
  FrownOutlined,
  HomeOutlined,
  VideoCameraOutlined,
  VideoCameraFilled,
} from "@ant-design/icons";
import useBaseHooks from "@root/src/hooks/BaseHook";
import deviceService from "@root/src/services/deviceService";
import { useRequest } from "ahooks";

const CameraTreeClick = () => {
  const { t, notify, setStore, getStore } = useBaseHooks();

  const [expandedKeys, setExpandedKeys] = useState(null);

  const { data: devices, loading: requestLoading } = useRequest(
    async () => {
      let res = await deviceService().withAuth().getDevice({});
      if (!expandedKeys) setExpandedKeys(res.map((device) => device.deviceID));
      return res;
    },
    {
      cacheKey: "devices",
    }
  );

  const currentViews = getStore("currentViews") || [];

  const treeData = (devices || []).map((parent) => {
    const parentOnline = parent.online === 1;

    let parentChildren = parent.channels.map((channel) => {
      const channelNo = Number(channel.logicChannel) + 1;
      const isOnline = channel.online === 1;

      return {
        title: (
          <span className={isOnline ? "" : "secondary"}>{channel.name}</span>
        ),
        key: parent.deviceID + "-" + channel.logicChannel,
        icon:
          currentViews.indexOf(channelNo) > -1 ? (
            <VideoCameraFilled />
          ) : (
            <VideoCameraOutlined />
          ),
      };
    });
    return {
      title: (
        <span className={parentOnline ? "" : "secondary"}>
          {parent.deviceName}
        </span>
      ),
      key: parent.deviceID,
      children: parentChildren,
      //   icon: <HomeOutlined />,
    };
  });

  const onSelect = (anyNode: EventDataNode) => {
    if (anyNode.children) {
      onExpand(anyNode);
    } else {
      let channel = (anyNode.key + "").split("-")[1];
      console.log(anyNode.key, Number(channel) + 1);
      const channelNo = Number(channel) + 1;
      let _views = [...currentViews];
      if (_views.indexOf(channelNo) === -1) {
        _views.push(channelNo);
      } else {
        _views.splice(_views.indexOf(channelNo), 1);
      }
      setStore("currentViews", _views);
    }
  };

  const onExpand = (anyNode: EventDataNode) => {
    if (anyNode.children) {
      if (anyNode.expanded) {
        setExpandedKeys((values) => {
          let _values = [...values];
          let findIndex = _values.findIndex((val) => val === anyNode.key);
          _values.splice(findIndex, 1);
          return _values;
        });
      } else setExpandedKeys((values) => [...values, anyNode.key + ""]);
    }
  };

  return (
    <Tree
      // checkable
      showIcon
      expandedKeys={expandedKeys}
      onDoubleClick={(e, node) => onSelect(node)}
      // onSelect={(keys, info) => console.log(keys)}
      onExpand={(keys, info) => onExpand(info.node)}
      treeData={treeData}
    />
  );
};

export default CameraTreeClick;
