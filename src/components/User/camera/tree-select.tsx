import { Tree, Typography } from "antd";
// import devices from "@root/public/static/devices.json";
import {
  DownOutlined,
  FrownOutlined,
  HomeOutlined,
  VideoCameraOutlined,
  FrownFilled,
} from "@ant-design/icons";
import { useRequest } from "ahooks";
import deviceService from "@root/src/services/deviceService";
import { useState } from "react";

const CameraTreeSelect = ({ onChange }) => {
  const [expandedKeys, setExpandedKeys] = useState(null);

  const { data: devices, loading: requestLoading } = useRequest(
    async () => {
      let res = await deviceService().withAuth().getDevice({});
      if (!expandedKeys) setExpandedKeys(res.map((device) => device.deviceID));
      return res;
    },
    {
      cacheKey: "devices",
    }
  );

  const treeData = (devices || []).map((parent) => {
    const parentOnline = parent.online === 1;

    let parentChildren = parent.channels.map((channel) => {
      const channelNo = Number(channel.logicChannel) + 1;
      const isOnline = channel.online === 1;
      return {
        title: (
          <span className={isOnline ? "" : "secondary"}>{channel.name}</span>
        ),
        key: parent.deviceID + "-" + channel.logicChannel,
        icon: <VideoCameraOutlined color={isOnline ? "" : "gray"} />,
        disableCheckbox: isOnline ? false : true,
      };
    });
    return {
      title: (
        <span className={parentOnline ? "" : "secondary"}>
          {parent.deviceName}
        </span>
      ),
      key: parent.deviceID,
      children: parentChildren,
      //   icon: <HomeOutlined />,
      disableCheckbox: parent.online === 1 ? false : true,
    };
  });

  const onCheck = (selectedKeys) => {
    if (onChange && typeof onChange === "function") {
      let selected = selectedKeys.filter((val) => val.indexOf("-") > 0);
      onChange(selected);
    }
  };

  const onSelect = (checkedKeys: React.Key[], info: any) => {
    // console.log("onCheck", checkedKeys, info);
  };

  return (
    <Tree
      checkable
      showIcon
      expandedKeys={expandedKeys}
      onSelect={onSelect}
      onCheck={onCheck}
      treeData={treeData || []}
    />
  );
};

export default CameraTreeSelect;
