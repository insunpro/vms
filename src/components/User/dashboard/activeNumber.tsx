import { Progress, Typography } from "antd";
import { useEffect, useState } from "react";

const ActiveNumber = ({ maxNumber }) => {
  const [activeNum, setActiveNum] = useState(0);

  const increaseActive = (curent) => {
    if (curent < maxNumber) {
      setActiveNum(curent + 1);
      setTimeout(() => {
        increaseActive(curent + 1);
      }, 100);
    }
  };

  useEffect(() => {
    increaseActive(activeNum);
  }, []);

  return (
    <div style={{ textAlign: "center" }}>
      <Progress
        type="circle"
        strokeColor={{
          "0%": "#fe7e17",
          "100%": "#87d068",
        }}
        percent={100}
      />
      <br />
      <Typography.Text style={{ color: "#f4f4f4" }}>
        Hoạt động: {`${activeNum}/${maxNumber}`}
      </Typography.Text>
    </div>
  );
};

export default ActiveNumber;
