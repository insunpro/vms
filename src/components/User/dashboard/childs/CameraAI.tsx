import { Badge, Tooltip } from "antd";
import React, { useEffect, useState } from "react";
import Marker from "./Marker";

const CameraAI = ({
  x,
  y,
  statusClass,
  statusText,
  label,
  onClick,
  children,
  markerProps,
  ...otherProps
}: {
  x: number;
  y: number;
  status: number;
  label: string;
  dragable?: boolean;
  onClick?: Function;
  markerProps?: any;
  [x: string]: any;
}) => {
  return (
    <div
      className={`camera-component ${statusClass}`}
      onClick={() =>
        onClick &&
        onClick({
          x,
          y,
          status,
          ...otherProps,
        })
      }
    >
      <Marker
        {...otherProps}
        defaultPosition={{
          x,
          y,
        }}
        bounds=".mapLayout"
        markerProps={{
          style: {
            width: "32px",
          },
        }}
      >
        <Tooltip
          title={
            <>
              <b>Camera {label}</b> <br />
              Trạng thái: {statusText}
            </>
          }
        >
          <img src="/images/camera-ai.png" alt="" />
          {/* <div className={`info`}>
            <span className="camera-label">{label}</span>
          </div> */}
        </Tooltip>
      </Marker>
    </div>
  );
};

export default CameraAI;
