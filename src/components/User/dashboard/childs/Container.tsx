import React from "react";

const Container = ({
  backgroundImage,
  children,
  style,
  containerClass,
  ...otherProps
}: {
  backgroundImage?: string;
  children?: any;
  style?: any;
  containerClass?: string;
  [x: string]: any;
}) => {
  return (
    <div className="mapLayout">
      <div
        className={`map-container ${containerClass}`}
        style={{
          backgroundImage: `url(${backgroundImage})`,
          ...style,
        }}
        {...otherProps}
      >
        {children}
      </div>
    </div>
  );
};

export default Container;
