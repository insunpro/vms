import React, { useEffect, useState } from "react";
import Draggable from "react-draggable";

const Marker = ({
  children,
  dragable,
  markerProps,
  defaultPosition,
  ...otherProps
}: {
  dragable?: boolean;
  markerProps?: any;
  [x: string]: any;
}) => {
  if (!dragable) otherProps["position"] = defaultPosition;
  return (
    <Draggable disabled={dragable == true ? false : true} {...otherProps}>
      <div style={{ position: "absolute" }}>
        <div className="map-marker" {...markerProps}>
          {children}
        </div>
      </div>
    </Draggable>
  );
};

export default Marker;
