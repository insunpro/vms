import React, { useCallback, useEffect, useState } from "react";
import Container from "./childs/Container";
import CameraAI from "./childs/CameraAI";

const markerTypes = {
  cameraAI: CameraAI,
  unknown: <></>,
};

export interface TechnicalDrawingProps {
  markers: any[];
  onClick?: () => any;
}

const TechnicalDrawing = ({ markers, onClick }: TechnicalDrawingProps) => {
  const [scale, setScale] = useState(1);

  const renderMarkers = () => {
    return markers?.map((marker, index) => {
      const Component = markerTypes[marker.type];
      return (
        <Component key={index} {...marker} onClick={onClick} dragable={false} />
      );
    });
  };

  const updateScale = useCallback(() => {
    let newScale = ((window.innerWidth * 16) / 24 - 100) / 800;
    setScale(newScale * 0.5);
  }, []);

  useEffect(() => {
    window.addEventListener("resize", updateScale);
    updateScale();

    return () => {
      window.removeEventListener("resize", updateScale);
    };
  }, []);
  return (
    <div
      style={{
        height: `${889 * scale}px`,
      }}
    >
      <div
        className="map-layout"
        style={{
          width: "1505px",
          height: "889px",
          transform: `scale(${scale})`,
        }}
      >
        <Container backgroundImage="/images/a05map.png">
          {renderMarkers()}
        </Container>
      </div>
    </div>
  );
};

export default TechnicalDrawing;
