import useBaseHooks from "@root/src/hooks/BaseHook";
import deviceService from "@root/src/services/deviceService";
import { Modal, Spin } from "antd";
import to from "await-to-js";
import moment from "moment";
import { useEffect, useState } from "react";
import ReactPlayer from "react-player";

const PlaybackModal = ({ visible, setVisible, record }) => {
  const [url, setUrl] = useState(null);
  const [loading, setLoading] = useState(false);
  const [err, setErr] = useState(null);
  const [playing, setPlaying] = useState(false);
  const { notify, t } = useBaseHooks();

  const onPreview = async (record) => {
    if (!record) return;
    setLoading(true);

    let times = [];
    let startTime = record.StartTime || record.Time;
    let endTime = record.EndTime || startTime;
    console.log(">>>>>>>>>> record", record, startTime, endTime);

    if (startTime === endTime) {
      const offsetTime = moment().utcOffset();
      console.log("offsetTime", offsetTime, startTime, endTime);
      startTime = moment(startTime, "YYYY-MM-DD HH:mm:ss")
        // .add(420 - offsetTime, "minute")
        .subtract(10, "seconds");
      // .utcOffset(420);
      endTime = moment(endTime, "YYYY-MM-DD HH:mm:ss").add(10, "seconds");
      // .add(420 - offsetTime, "minute");

      // .utcOffset(420);
      let [err, findFiles] = await to(
        deviceService()
          .withAuth()
          .scanVideo({
            channels: [Number(record.Channel)],
            startTime: startTime.clone().toISOString(),
            endTime: endTime.clone().toISOString(),
          })
      );
      if (err || !findFiles.data || !findFiles.data.length) {
        setLoading(false);
        return notify("cannot find files", "", "error");
      }
      if (findFiles.data.length === 1) {
        times.push({
          startTime: startTime.format("YYYY-MM-DD HH:mm:ss"),
          endTime: endTime.format("YYYY-MM-DD HH:mm:ss"),
        });
      } else {
        findFiles.data.forEach((file, index) => {
          if (index === 0) {
            times.push({
              startTime: startTime.format("YYYY-MM-DD HH:mm:ss"),
              endTime: file.EndTime,
            });
          } else if (index === findFiles.data.length - 1) {
            times.push({
              startTime: file.StartTime,
              endTime: endTime.format("YYYY-MM-DD HH:mm:ss"),
            });
          } else {
            times.push({
              startTime: file.StartTime,
              endTime: file.EndTime,
            });
          }
        });
      }
    } else {
      times.push({ startTime, endTime });
    }
    console.log(">>>>>>>>>> times", times);
    let [err, res] = await to(
      deviceService()
        .withAuth()
        .getPlayback({
          channel: Number(record.Channel) + 1,
          times,
        })
    );
    setLoading(false);
    if (err) {
      //@ts-ignore
      return notify(t(`errors:${err.code}`), err.message || "", "error");
    }
    console.log("url", res);
    setUrl(res);
    setTimeout(() => {
      setPlaying(true);
    });
  };

  useEffect(() => {
    setUrl(null);
    onPreview(record);
  }, [record]);

  return (
    <Modal
      visible={visible}
      onCancel={() => {
        // setUrl(null);
        setVisible(false);
        setPlaying(false);
      }}
      // destroyOnClose={true}
      footer={false}
      width={800}
      bodyStyle={{
        backgroundColor: "#333333",
      }}
      wrapClassName="user-stream-modal"
    >
      <div style={{ marginTop: 20 }}>
        {url && visible ? (
          <ReactPlayer
            url={url}
            controls
            width={"100%"}
            height={"100%"}
            style={{ lineHeight: 0 }}
            playing={playing}
          />
        ) : (
          <Spin
            tip="Loading..."
            style={{ backgroundColor: "rgba(0,0,0,0)" }}
            spinning={loading}
          >
            <video
              height="100%"
              width="100%"
              controls
              style={{ lineHeight: 0 }}
            ></video>
          </Spin>
        )}
      </div>
    </Modal>
  );
};

export default PlaybackModal;
