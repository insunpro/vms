import { Button, Modal, Radio } from "antd";
import _ from "lodash";
import React, { useEffect, useState } from "react";
import ReactPlayer from "react-player";

interface Camera {
  host: string;
}

interface StreamModalProps {
  visible: boolean;
  camera: Camera;
  onClose: () => any;
}

export default function StreamModal({
  visible,
  camera,
  onClose,
}: StreamModalProps) {
  const SRCS = ["videos/vms-sample-1.mp4"];
  const [url, setUrl] = useState(null);
  useEffect(() => {
    setUrl(`${window.location.protocol}//${window.location.host}/${SRCS[0]}`);
  }, [camera]);

  return (
    <Modal
      visible={visible}
      title={"Xem trực tiếp - " + camera?.host}
      footer={false}
      // destroyOnClose={true}
      onCancel={() => {
        onClose();
      }}
      width={800}
      bodyStyle={{
        backgroundColor: "#333333",
      }}
      wrapClassName="user-stream-modal"
    >
      {camera && (
        <div
          style={{
            display: "flex",
            backgroundColor: "#333333",
          }}
        >
          <ReactPlayer
            url={url}
            playing={true}
            style={{
              flex: 1,
              alignSelf: "center",
              // objectFit: "fill",
            }}
          />
        </div>
      )}
    </Modal>
  );
}
