import { PeerMediaStream } from "@root/src/services/p2p";
import { Button, Spin } from "antd";
import { to } from "await-to-js";
import { useEffect, useState } from "react";
import ReactPlayer from "react-player";

const PeerPlayer = ({ peer, cameraId }) => {
  const [stream, setStream] = useState(null);
  const [status, setStatus] = useState("loading");

  useEffect(() => {
    getStream();
    console.log(cameraId, ">>>", peer);
  }, [peer, cameraId]);

  const getStream = async () => {
    if (!peer || !cameraId) return false;

    let [err, mediaStream] = await to(
      PeerMediaStream.connect({
        peer: peer,
        cameraId: cameraId,
      })
    );
    if (err) {
      setStatus("error");
      return console.error("PeerPlayer.getStream", err);
    }
    setStream(mediaStream);
    setStatus("playing");
  };

  return (
    <Spin spinning={status === "loading"}>
      <div className="player-wrapper">
        <ReactPlayer
          className="react-player"
          url={stream || ""}
          key={cameraId}
          playing={status === "playing"}
          width="100%"
          height="100%"
          style={{ lineHeight: 0 }}
          controls
          // onProgress={() => setStatus("playing")}
          // fallback={<video width="100%" height="100%" controls />}
        />
        {status === "error" && (
          <div className="player-error">
            <Button>Reset</Button>
          </div>
        )}
      </div>
    </Spin>
  );
};

export default PeerPlayer;
