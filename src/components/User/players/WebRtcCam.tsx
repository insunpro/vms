import deviceService from "@root/src/services/deviceService";
import { Spin } from "antd";
import to from "await-to-js";
import _ from "lodash";
import React from "react";
import { useEffect, useRef, useState } from "react";

interface VideoControls {
  muteButton?: boolean;
  playButton?: boolean;
  timeline?: boolean;
  fullscreenButton?: boolean;
  currentTime?: boolean;
  fullscreenButtonTop?: boolean;
}

const WebRtcCam = ({
  cameraHost,
  config = {},
  onLoadComplete,
  channel = "main",
  controls = {
    muteButton: false,
    playButton: false,
    timeline: false,
    fullscreenButton: false,
    fullscreenButtonTop: false,
    currentTime: false,
  },
}: {
  cameraHost: string;
  config?: any;
  onLoadComplete?: any;
  channel?: "main" | "sub";
  controls?: VideoControls;
}) => {
  const [loading, setLoading] = useState(true);

  const player = useRef(null);

  useEffect(() => {
    let stream = new MediaStream();
    const pc = new RTCPeerConnection(config);
    const handleNegotiationNeededEvent = async () => {
      let offer = await pc.createOffer();
      await pc.setLocalDescription(offer);
      getRemoteSdp();
    };

    pc.onnegotiationneeded = handleNegotiationNeededEvent;

    pc.ontrack = (event) => {
      stream.addTrack(event.track);
      if (player.current) {
        player.current.srcObject = stream;
        setLoading(false);
      }
      setLoading(false);
    };

    const getRemoteSdp = async () => {
      let [err, data]: any = await to(
        deviceService()
          .withAuth()
          .webrtcSdp({
            host: cameraHost,
            channel: channel,
            sdp: btoa(pc.localDescription.sdp),
          })
      );
      if (err) {
        return console.log("getRemoteSdp", err);
      }
      pc.setRemoteDescription(
        new RTCSessionDescription({
          type: "answer",
          sdp: atob(data),
        })
      );
      setLoading(false);
    };

    const getCodecInfo = async () => {
      let [err, data]: any = await to(
        deviceService().withAuth().webrtcCodec({
          host: cameraHost,
          channel: channel,
        })
      );
      if (err) {
        return console.log("getRemoteSdp", err);
      }
      data.forEach((codec) => {
        pc.addTransceiver(codec.Type, {
          direction: "sendrecv",
        });
      });
    };

    getCodecInfo();
    return () => {
      pc.close();
    };
  }, [cameraHost]);

  useEffect(() => {
    onLoadComplete && onLoadComplete({ width: 1920, height: 1080 });
  }, []);
  //console.log("camera re render")

  const getVideoWrapper = () => {
    let wrappers = [
      "video-wrapper",
      "play-button-off",
      "mute-button-off",
      "current-time-off",
      "timeline-off",
      "fullscreen-button-off",
    ];
    if (!controls) return wrappers.join(" ");

    if (controls.currentTime)
      _.remove(wrappers, (str) => str === "current-time-off");
    if (controls.playButton)
      _.remove(wrappers, (str) => str === "play-button-off");
    if (controls.timeline) _.remove(wrappers, (str) => str === "timeline-off");
    if (controls.fullscreenButton)
      _.remove(wrappers, (str) => str === "fullscreen-button-off");
    if (controls.muteButton)
      _.remove(wrappers, (str) => str === "mute-button-off");

    if (controls.fullscreenButtonTop) wrappers.push("fullscreen-button-top");

    return wrappers.join(" ");
  };
  return (
    <>
      {loading && (
        <div
          style={{ minHeight: "100%", backgroundColor: "#000", margin: "0" }}
        >
          <Spin style={{ paddingTop: "25%", paddingLeft: "48%" }} />
        </div>
      )}
      {!loading && (
        <div className={getVideoWrapper()}>
          <video
            ref={player}
            style={{ width: "100%" }}
            disablePictureInPicture
            autoPlay
            muted
            controls
          >
            <p>no support</p>
          </video>
        </div>
      )}
    </>
  );
};

export default React.memo(WebRtcCam, (prevProps, nextProps) => {
  if (prevProps.cameraHost != nextProps.cameraHost) {
    return false;
  }
  return true;
});
