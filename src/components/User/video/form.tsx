import { addQuery } from "@root/src/helpers/routes";
import { Button, Form, Input, Upload } from "antd";

interface VideoFormProps {
  initialValue?: object;
}

const VideoForm = ({ initialValue }: VideoFormProps) => {
  return (
    <Form layout="vertical" wrapperCol={{ xs: { span: 12, offset: 6 } }}>
      <Form.Item label="name">
        <Input />
      </Form.Item>
      <Form.Item>
        <Upload>
          <Button>Upload file</Button>
        </Upload>
      </Form.Item>
      <Form.Item label="tag">
        <Input />
      </Form.Item>
      <Form.Item>
        <Button type="primary" htmlType="submit">
          Submit
        </Button>
        <Button onClick={() => addQuery({ action: "" }, true)}>Cancel</Button>
      </Form.Item>
    </Form>
  );
};

export default VideoForm;
