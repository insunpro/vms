import {
  Avatar,
  Button,
  Card,
  Col,
  Divider,
  List,
  Row,
  Typography,
} from "antd";
import Search from "antd/lib/input/Search";
import { useEffect, useState } from "react";
import { VideoCameraOutlined } from "@ant-design/icons";
import videos from "@root/public/static/videos.json";
import ReactPlayer from "react-player";
import VideoPlayer from "./player";
export interface VideoGridProps {
  onSelect?: () => any;
}

const VideoGrid = ({ onSelect }: VideoGridProps) => {
  const [data, setData] = useState([]);
  const [selected, setSelected] = useState(null);

  useEffect(() => {
    // appendData();

    setTimeout(() => {
      setData(videos.RECORDS);
    }, 300);
  }, []);

  return (
    <>
      <Row gutter={[4, 4]}>
        {data.map((video) => {
          return (
            <Col xs={12} sm={8} md={6} key={video.path}>
              <div style={{ border: "solid 1px #000", background: "#000" }}>
                <ReactPlayer
                  url={"/" + video.path}
                  width="100%"
                  height="100%"
                  playing={false}
                  controls
                />
                <div style={{ textAlign: "center" }}>
                  <Typography.Text style={{ color: "#f4f4f4" }}>
                    {video.name}
                  </Typography.Text>
                </div>
              </div>
            </Col>
          );
        })}
      </Row>
    </>
  );
};

export default VideoGrid;
