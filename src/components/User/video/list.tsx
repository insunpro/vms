import {
  Avatar,
  Button,
  Card,
  Col,
  Divider,
  Input,
  List,
  Row,
  Typography,
} from "antd";
import Search from "antd/lib/input/Search";
import { useEffect, useState } from "react";
import cameras from "@root/public/static/cameras.json";
import { LoadingOutlined, SearchOutlined } from "@ant-design/icons";
import videos from "@root/public/static/videos.json";
import ReactPlayer from "react-player";
import VideoPlayer from "./player";
import _ from "lodash";
export interface VideoListProps {
  onSelect?: (data) => any;
  reset?: boolean;
}

const VideoList = ({ onSelect, reset }: VideoListProps) => {
  const [data, setData] = useState([]);
  const [selected, setSelected] = useState(null);
  const [searching, setSearching] = useState(false);

  useEffect(() => {
    setTimeout(() => {
      setData(videos.RECORDS);
    }, 300);
  }, []);

  const handleSearch = (searchValue) => {
    let newData;
    if (!searchValue) {
      newData = videos.RECORDS;
    } else {
      newData = data.filter(
        (val) =>
          val.name.toLowerCase().indexOf(searchValue.toLowerCase()) !== -1
      );
    }
    setData(newData);
    setSearching(false);
  };

  const debounceSearch = _.debounce(handleSearch, 750);

  useEffect(() => {
    setSelected(null);
  }, [reset]);

  return (
    <>
      <div>
        <Input
          placeholder="Tìm kiếm"
          onChange={(e) => {
            setSearching(true);
            debounceSearch(e.target.value);
          }}
          prefix={searching ? <LoadingOutlined /> : <SearchOutlined />}
        />
      </div>
      <br />
      <div className="list-item">
        <List className="input-bg list-video">
          {data.map((item) => (
            <List.Item
              key={item.id}
              className={`select-list-item ${
                item.id === selected ? "active" : ""
              }`}
              onClick={() => {
                setSelected(item.id);
                onSelect && onSelect(item);
              }}
            >
              <div>
                <div style={{ textAlign: "center" }}>
                  <Typography.Text className="video-list-title">
                    {item.name}
                  </Typography.Text>
                </div>
                <div style={{ lineHeight: 0, marginTop: 12 }}>
                  <ReactPlayer
                    url={"/" + item.path}
                    width="100%"
                    height="100%"
                    playing={false}
                    // controls
                  />
                </div>
              </div>
            </List.Item>
          ))}
        </List>
      </div>
    </>
  );
};

export default VideoList;
