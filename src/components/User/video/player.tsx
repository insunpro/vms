import ReactPlayer from "react-player";

const VideoPlayer = ({ url, cam }) => {
  return (
    <div
      style={{
        border: "solid 1px #000",
        height: "100%",
        display: "flex",
        flexDirection: "column",
        position: "relative",
      }}
    >
      <div
        style={{
          height: "100%",
          display: "flex",
          flexDirection: "column",
          justifyContent: "space-around",
          background: "#000",
        }}
      >
        <ReactPlayer
          url={url}
          playing={true}
          style={
            {
              // flex: 1,
              // alignSelf: "center",
              // objectFit: "fill",
            }
          }
          width={"100%"}
          height={"100%"}
        />
      </div>
      <div
        style={{
          height: "35px",
          position: "absolute",
          width: "100%",
          bottom: "0",
        }}
      >
        <div
          style={{
            padding: "5px 10px",
            position: "absolute",
            width: "100%",
            height: "100%",
            top: 0,
            left: 0,
            zIndex: 2,
          }}
        >
          {/* <Tooltip title={label}> */}
          {/* <span className={`status status-${status} status-big`}></span> */}
          <span>{cam.name}</span>
          {/* </Tooltip> */}
        </div>
        <div
          style={{
            background: "#fff",
            opacity: "0.5",
            position: "absolute",
            width: "100%",
            height: "100%",
            top: "0",
            left: 0,
            zIndex: 0,
          }}
        ></div>
      </div>
    </div>
  );
};

export default VideoPlayer;
