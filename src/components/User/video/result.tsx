import { Button, Card, Col, Grid, Image, Row, Spin } from "antd";
import { EyeOutlined } from "@ant-design/icons";
import { useEffect, useState } from "react";
import ModalPlayback from "../modal/Playback";
import to from "await-to-js";
import deviceService from "@root/src/services/deviceService";
import getConfig from "next/config";
import PlaybackResultCard from "./resultCard";
import { useRequest } from "ahooks";

const { publicRuntimeConfig } = getConfig();

const grids = [
  "",
  "ant-col-24",
  "ant-col-12",
  "ant-col-8",
  "ant-col-6",
  "ant-col-55",
  "ant-col-4",
];

const VideoResult = ({ data, total, loading }) => {
  const [visible, setVisible] = useState(false);
  const [preview, setPreview] = useState(null);
  const [_loading, setLoading] = useState(loading);
  const screens = Grid.useBreakpoint();

  const { data: devices, loading: requestLoading } = useRequest(
    async () => deviceService().withAuth().getDevice({}),
    {
      cacheKey: "devices",
    }
  );

  const channelNames = {};
  (devices || []).map((device) => {
    device.channels.map((channel) => {
      channelNames[Number(channel.logicChannel) + 1] = channel.name;
    });
  });

  useEffect(() => {
    setLoading(loading);
  }, [loading]);
  return (
    <Card
      className="card-container"
      title={`Kết quả tìm kiếm ${total < 0 ? "" : `(${total} kết quả)`}`}
      style={{ height: "calc(100vh - 112px)" }}
    >
      <Spin spinning={_loading}>
        <div className="analysis-result">
          <Row
            gutter={[4, 4]}
            style={{ height: "calc(100vh - 212px)", overflowY: "scroll" }}
          >
            {data.map((record, index) => {
              return (
                <Col className={screens.sm ? grids[5] : grids[6]} key={index}>
                  <PlaybackResultCard
                    title={
                      channelNames[Number(record.Channel) + 1] || record.Channel
                    }
                    data={record}
                    onPlayback={(data) => {
                      setVisible(true);
                      setPreview(data);
                    }}
                  />
                </Col>
              );
            })}
          </Row>
        </div>
      </Spin>
      <ModalPlayback
        visible={visible}
        setVisible={setVisible}
        record={preview}
      />
    </Card>
  );
};

export default VideoResult;
