import {
  FileSearchOutlined,
  PlayCircleOutlined,
  SmileOutlined,
  SolutionOutlined,
  UserOutlined,
  VideoCameraOutlined,
} from "@ant-design/icons";
import {
  botOptions,
  colorOptions,
  faceAgeOptions,
  genderOptions,
  glassesOptions,
  maskOptions,
  topOptions,
} from "@root/config/constant";
import { Col, Row, Image, Button, Typography, Popover, Tooltip } from "antd";
import _ from "lodash";
import moment from "moment";
import getConfig from "next/config";
import { useState } from "react";

const { publicRuntimeConfig } = getConfig();

const PlaybackResultCard = ({ title, data, onPlayback = null }) => {
  let queryStr = btoa(
    JSON.stringify({
      path: data.FilePath,
      time: data.StartTime,
    })
  );
  return (
    <div className="result-card">
      <div className="result-card-header">
        <div className="result-card-header-title">
          <Typography.Text style={{ maxWidth: 130 }} ellipsis>
            <VideoCameraOutlined />
            {title}
          </Typography.Text>
        </div>
        <div className="result-card-header-extra"></div>
      </div>
      <div className="result-card-body">
        <div className="result-card-body-content">
          <Image
            src={`${publicRuntimeConfig.API_HOST}/api/v1/img2?path=${queryStr}`}
            height="100%"
            preview={{
              visible: false,
              mask: <span>Xem</span>,
              onVisibleChange: (value) => {
                onPlayback && onPlayback(data);
              },
            }}
          />
        </div>
        {/* <div className="result-card-body-content">content</div> */}
      </div>
      <div className="result-card-footer">
        <div className="result-card-footer-time">
          {moment(data.StartTime, "YYYY-MM-DD HH:mm:ss").format("HH:mm:ss")} -{" "}
          {moment(data.EndTime, "YYYY-MM-DD HH:mm:ss").format("HH:mm:ss")}
        </div>
      </div>
    </div>
  );
};

export default PlaybackResultCard;
