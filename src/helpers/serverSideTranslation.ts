import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import nextI18NextConfig from "@root/next-i18next.config";

export default async (context) => {
    const { locale, defaultLocale } = context;
    return await serverSideTranslations(
      locale || defaultLocale,
      undefined,
      nextI18NextConfig
    );
  };