import { useRouter } from "next/router";
import { useDispatch, useSelector } from "react-redux";
import { setStore as setStoreAction } from "@src/components/Redux/Store";
import _ from "lodash";
import { notification } from "antd";
import { makeUrl, getRouteData } from "@src/helpers/routes";
import { useTranslation, i18n } from "next-i18next";

// interface BaseBook {
//   useSelector: Function;
//   router: any;
//   t: Function;
//   setStore: Function;
//   getStore: Function;
//   redirect: Function;
//   getData: Function;
//   notify: Function;
//   sprintf: Function;
// }
const useBaseHooks = ({ lang = ["common"] }: { lang?: string[] } = {}) => {
  const router = useRouter();
  const dispatch = useDispatch();
  const { t } = useTranslation(lang);
  //console.log(i18n)
  //i18n.addResourceBundle(locale, '<namespace name>')

  const setStore = async (path: string, value: any): Promise<any> => {
    return dispatch(setStoreAction(path, value));
  };

  const getStore = (path: string): any => {
    return useSelector((state: any) => _.get(state, path));
  };

  const redirect = async (
    url: string,
    query: any = null,
    shallow: boolean = false
  ) => {
    const routeData = await getRouteData();
    let nextRoute;
    try {
      nextRoute = makeUrl(url, query, routeData);
    } catch (e) {
      console.log(url);
      nextRoute = {
        href: url,
        as: url,
      };
    }

    router.push(nextRoute.href, nextRoute.as, {
      shallow,
    });
  };

  const getData = (
    obj: any,
    path: string,
    defaultValue: any = undefined
  ): any => {
    let value = _.get(obj, path, defaultValue);
    if (value == null) return defaultValue;
    return value;
  };

  const notify = (
    message: string,
    description: string = "",
    type: "success" | "error" | "warning" = "success"
  ): any => {
    notification[type]({
      message: message,
      description: description,
      duration: 4, //second
    });
    return <></>;
  };

  const sprintf = (message: string, keys: any) => {
    const regexp = /{{([^{]+)}}/g;
    let result = message.replace(regexp, function (ignore, key) {
      return (key = keys[key]) == null ? "" : key;
    });
    return result;
  };

  return {
    useSelector,
    router,
    t,
    setStore,
    getStore,
    redirect,
    getData,
    notify,
    sprintf,
  };
};

useBaseHooks.getData = (
  obj: any,
  path: string,
  defaultValue: any = undefined
): any => {
  let value = _.get(obj, path, defaultValue);
  if (value == null) return defaultValue;
  return value;
};

export default useBaseHooks;
