import React, { useState, useEffect } from "react";
import {
  Layout,
  Drawer,
  BackTop,
  Row,
  Col,
  Typography,
  Button,
  Avatar,
} from "antd";
import useBaseHooks from "@src/hooks/BaseHook";
import usePermissionHook from "@src/hooks/PermissionHook";

import Head from "next/head";
import dynamic from "next/dynamic";
import getConfig from "next/config";
import useSWR from "swr";
import Error403 from "@src/components/Errors/403";
const Sidebar = dynamic(() => import("./user/Sidebar"), { ssr: false });
// const Header = dynamic(() => import("./user/Header"), { ssr: false });
const BreadCrumb = dynamic(() => import("@src/components/BreadCrumb"), {
  ssr: false,
});
import {
  HomeOutlined,
  UserOutlined,
  UserSwitchOutlined,
  SettingOutlined,
} from "@ant-design/icons";

import { getRouteData } from "@src/helpers/routes";
import { Header } from "antd/lib/layout/layout";
import Sider from "antd/lib/layout/Sider";
import SubMenu from "antd/lib/menu/SubMenu";
import Menu from "./user/Menu";

const THEME = "light";
const { publicRuntimeConfig } = getConfig();
const { Title, Text } = Typography;
const { Content, Footer } = Layout;

const Admin = (props: any) => {
  const { router, t, setStore } = useBaseHooks();
  const { checkPermission } = usePermissionHook();
  const [collapsed, setCollapsed] = useState(false);
  const [isMobile, setIsMobile] = useState(false);
  const onCollapseChange = (value: boolean) => {
    setCollapsed(value);
  };

  const updateSize = () => {
    const mobile = window.innerWidth < 992;
    setIsMobile(mobile);
    setStore("isMobile", mobile);
    setCollapsed(false);
  };

  useEffect(() => {
    window.addEventListener("resize", updateSize);
    updateSize();
    return () => window.removeEventListener("resize", updateSize);
  }, []);

  if (!checkPermission(props.permissions || {})) {
    return <Error403 />;
  }

  const getRouteName = async () => {
    const routePath = router.pathname;
    const routeData: any = await getRouteData();
    for (let routeName in routeData) {
      let routeElement = routeData[routeName];
      if (!routeElement.action) continue;
      if (routeElement.action.substr(5) === routePath) return routeName;
    }
  };

  const { data: routeName } = useSWR("getRouteName", () => getRouteName());

  return (
    <>
      <Head>
        <title>{props.title || publicRuntimeConfig.SITE_NAME || ""}</title>
        <meta
          property="og:title"
          content={props.title || publicRuntimeConfig.TITLE || ""}
        />
        <meta
          property="og:description"
          content={props.description || publicRuntimeConfig.DESCRIPTION || ""}
        />
        <link
          rel="shortcut icon"
          type="image/png"
          href={publicRuntimeConfig.FAVICON}
        />
        <meta property="og:image" content={publicRuntimeConfig.LOGO} />
        <link rel="apple-touch-icon" href={publicRuntimeConfig.LOGO_IOS}></link>
      </Head>
      <div id="user">
        <Layout>
          <Header className="header">
            {/* <div className="logo" /> */}
            <Menu />
          </Header>
          <Layout>
            <Content className="content">{props.children}</Content>
          </Layout>
        </Layout>
      </div>
    </>
  );
};

export default Admin;
