import {
  HomeOutlined,
  UserOutlined,
  TeamOutlined,
  AndroidOutlined,
  FieldNumberOutlined,
  CloudUploadOutlined,
  FieldBinaryOutlined,
  UnorderedListOutlined,
  CheckSquareOutlined,
  AuditOutlined,
  SettingOutlined,
} from "@ant-design/icons";

const sidebar = [
  {
    routeName: "frontend.admin.dashboard.index",
    icon: <HomeOutlined />,
    routeParams: {},
  },
  {
    routeName: "frontend.admin.users.index",
    icon: <UserOutlined />,
    routeParams: {},
    permissions: {
      admins: "R",
    },
  },
  {
    routeName: "frontend.admin.settings.index",
    icon: <SettingOutlined />,
    routeParams: {},
    permissions: {
      settings: "R",
    },
  },
];

export default sidebar;
