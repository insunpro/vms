import React, { useState, useEffect } from "react";
import { Avatar, Button, Col, Menu, Row, Spin } from "antd";
const { SubMenu } = Menu;
import sidebar from "./slidebar.config";
import useBaseHook from "@src/hooks/BaseHook";
import Link from "next/link";
import useSWR from "swr";
import usePermissionHook from "@src/hooks/PermissionHook";
import {
  makeUrl,
  getSidebarSelecteds,
  getRouteData,
} from "@src/helpers/routes";
import auth from "@src/helpers/auth";
import { UserSwitchOutlined, SettingOutlined } from "@ant-design/icons";
import authService from "@root/src/services/authService";

const MenuComponent = (props: any) => {
  const { user } = auth();
  const { theme, onCollapseChange, isMobile, tReady, ...otherProps } = props;
  const { router, t } = useBaseHook({ lang: ["menu"] });
  const { checkPermission } = usePermissionHook();
  const [routerNames, setRouterNames] = useState(undefined);
  const { data: routeData } = useSWR(["getRouteData"], () => getRouteData());
  const getRouteName = () => {
    const routePath = router.pathname;
    for (let routeName in routeData) {
      let routeElement = routeData[routeName];
      if (!routeElement.action) continue;
      if (routeElement.action.substr(5) === routePath) return routeName;
    }
  };
  const { getData, redirect } = useBaseHook();

  const currentRouteName = getRouteName();
  const { data: breadcums } = useSWR(
    Object.values(routeData || {}).length
      ? ["getSidebarSelecteds", currentRouteName, sidebar, routeData]
      : null,
    () => getSidebarSelecteds(currentRouteName, sidebar, routeData)
  );
  useEffect(() => {
    let routerNamesT = (breadcums || []).map(
      (breadcum: any) => breadcum.routeName
    );
    setRouterNames(routerNamesT);
  }, [breadcums]);

  const generateMenus = (data: any) => {
    return data
      .map((item: any) => {
        if (item.children) {
          if (item.type === "group") {
            let children = generateMenus(item.children);
            if (!children.length) return;
            return (
              <Menu.ItemGroup
                key={item.routeName}
                title={
                  <>
                    {item.icon ? item.icon : ""}
                    <span>{t(item.routeName)}</span>
                  </>
                }
              >
                {children}
              </Menu.ItemGroup>
            );
          } else {
            let children = generateMenus(item.children);
            if (!children.length) return;
            return (
              <SubMenu
                key={item.routeName}
                title={
                  <>
                    {item.icon ? item.icon : ""}
                    <span>{t(item.routeName).toUpperCase()}</span>
                  </>
                }
              >
                {children}
              </SubMenu>
            );
          }
        }

        // if(!["admin"].includes(user.type) && item.routeName === "frontend.admin.dashboard.index") return
        if (!checkPermission(item.permissions)) return;
        let routeInfo = makeUrl(
          item.routeName,
          item.routeParams,
          routeData || {}
        );
        const routeLang = {
          href: routeInfo.href || "",
          as: routeInfo.as || "",
        };
        return (
          <Menu.Item key={item.routeName}>
            <Link {...routeLang}>
              <a href={routeLang.as}>
                {item.icon ? item.icon : ""}
                <span>{t(item.routeName)}</span>
              </a>
            </Link>
          </Menu.Item>
        );
      })
      .filter((menu: any) => menu);
  };

  const firstName = getData(auth(), "user.firstName", "User");
  const username = getData(auth(), "user.username", "User");

  return (
    <Row>
      <Col flex="100px">
        <img src="/logo/dhqg-logo-3.png" height={64} />
      </Col>
      <Col flex="auto">
        {routerNames && routerNames.length > 0 ? (
          <Menu
            className="nav-menu"
            mode="horizontal"
            theme="dark"
            defaultOpenKeys={routerNames}
            selectedKeys={[...routerNames].pop()}
          >
            {generateMenus(sidebar)}
          </Menu>
        ) : (
          <Spin className="spin" />
        )}
      </Col>

      <Col flex="200px">
        <Menu className="nav-menu" mode="horizontal" theme="dark">
          {/* <Menu.Item key="user-info" className="user-info">
            
          </Menu.Item> */}
          <SubMenu
            title={
              <>
                <span className="crop-name">
                  {t("common:hi", { username: username })}
                </span>
                <Avatar
                  style={{ marginLeft: 8 }}
                  size="default"
                  shape="circle"
                  src={"https://joeschmoe.io/api/v1/random"}
                />
              </>
            }
            key="user-info"
            className="user-info"
          >
            {/* <Menu.Item key="change-password">
              <Button
                type="link"
              >
                {t("buttons:changePassword")} <SettingOutlined />
              </Button>
            </Menu.Item> */}
            <Menu.Item key="signout">
              <Button
                type="link"
                onClick={() => {
                  authService().withAuth().logout({ username });
                  redirect("frontend.login");
                }}
              >
                {t("buttons:signout")} <UserSwitchOutlined />
              </Button>
            </Menu.Item>
          </SubMenu>
        </Menu>
      </Col>

      {/* <Col span={24}>
        <div className="logo-middle">

        </div>
        <span></span>
        <div style={{ display: "inline-block", float: "right", width: 300 }}>
          
        </div>
      </Col> */}
    </Row>
  );
  // return <Menu
  //   mode="inline"
  //   theme={theme}
  //   className="side-bar"
  //   defaultOpenKeys={routerNames}
  //   selectedKeys={[...routerNames].pop()}
  //   onClick={
  //     isMobile
  //       ? () => {
  //         onCollapseChange(true)
  //       }
  //       : undefined
  //   }
  //   {...otherProps}
  // >
  //   {generateMenus(sidebar)}
  // </Menu>
};

export default MenuComponent;
