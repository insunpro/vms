import {
  HomeOutlined,
  VideoCameraAddOutlined,
  CameraOutlined,
  FileSearchOutlined,
} from "@ant-design/icons";

const sidebar = [
  {
    routeName: "frontend.user.dashboard.index",
    icon: <HomeOutlined />,
    routeParams: {},
  },
  {
    routeName: "frontend.user.cameras.index",
    icon: <CameraOutlined />,
    routeParams: {},
  },
  {
    routeName: "frontend.user.videos.index",
    icon: <VideoCameraAddOutlined />,
    routeParams: {},
  },
  {
    routeName: "frontend.user.analysis.index",
    icon: <FileSearchOutlined />,
    routeParams: {},
  },
];

export default sidebar;
