import React from "react";
import App from "next/app";
import { appWithTranslation } from "next-i18next";
import nextI18nextConfig from "@root/next-i18next.config";
import wrapper from "@src/components/Redux";
import "@src/less/base/custom-ant-theme.less";
import "@src/less/base/vars.less";
import "@src/less/default.less";
import "@src/less/login.less";
import "@src/less/admin.less";
import "@src/less/user.less";
import Router from "next/router";
import NProgress from "nprogress"; //nprogress module
import "nprogress/nprogress.css"; //styles of nprogress
import viVn from "antd/lib/locale/vi_VN";
import { ConfigProvider } from "antd";

//Binding events.
Router.events.on("routeChangeStart", () => NProgress.start());
Router.events.on("routeChangeComplete", () => NProgress.done(true));
Router.events.on("routeChangeError", () => NProgress.done());
const DefaultComponent = (props: any) => {
  return <>{props.children}</>;
};
class MyApp extends App {
  render() {
    const { Component, pageProps } = this.props;
    let Layout = Component["Layout"] ? Component["Layout"] : DefaultComponent;
    const permissions = Component["permissions"] || {};
    return (
      <Layout permissions={permissions}>
        <ConfigProvider locale={viVn}>
          <Component {...pageProps} />
        </ConfigProvider>
      </Layout>
    );
  }
}

export default wrapper.withRedux(appWithTranslation(MyApp, nextI18nextConfig));
