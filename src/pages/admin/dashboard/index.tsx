import dynamic from "next/dynamic";
const Layout = dynamic(() => import("@src/layouts/Admin"), { ssr: false });
import useBaseHook from "@src/hooks/BaseHook";
import usePermissionHook from "@src/hooks/PermissionHook";
import { Row, Col, Empty } from "antd";
import serverSideTranslation from "@root/src/helpers/serverSideTranslation";

const Dashboard = () => {
  const { checkPermission } = usePermissionHook();
  const { redirect } = useBaseHook();
  const dashboardPer = checkPermission({
    dashboard: "R",
  });
  if (!dashboardPer) {
    redirect("frontend.admin.users.index");
    return <></>;
  }

  return (
    <>
      <div className="content">
        <Empty />
      </div>
    </>
  );
};

Dashboard.Layout = (props) => {
  const { t } = useBaseHook();
  return (
    <Layout
      title={t("pages:dashboard.index.title")}
      description={t("pages:dashboard.index.description")}
      {...props}
    />
  );
};

export const getStaticProps = async (context) => {
  return {
    props: {
      ...(await serverSideTranslation(context)),
    },
  };
};

export default Dashboard;
