import React, { useRef } from "react";
import dynamic from "next/dynamic";
const Layout = dynamic(() => import("@src/layouts/Admin"), { ssr: false });
import useBaseHook from "@src/hooks/BaseHook";
import usePermissionHook from "@src/hooks/PermissionHook";
import { Empty } from "antd";
import serverSideTranslation from "@root/src/helpers/serverSideTranslation";

const Index = () => {
  const { t, notify, redirect } = useBaseHook();
  const tableRef = useRef(null);
  const { checkPermission } = usePermissionHook();
  const updatePer = checkPermission({
    settings: "U",
  });

  return (
    <>
      <div className="content">
        <Empty />
      </div>
    </>
  );
};

Index.Layout = (props) => {
  const { t } = useBaseHook();
  return (
    <Layout
      title={t("pages:settings.index.title")}
      description={t("pages:settings.index.description")}
      {...props}
    />
  );
};

Index.permissions = {
  settings: "R",
};

export const getStaticProps = async (context) => {
  return {
    props: {
      ...(await serverSideTranslation(context)),
    },
  };
};

export default Index;
