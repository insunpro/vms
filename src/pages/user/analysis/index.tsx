import dynamic from "next/dynamic";
const Layout = dynamic(() => import("@src/layouts/User"), { ssr: false });
import useBaseHook from "@src/hooks/BaseHook";
import usePermissionHook from "@src/hooks/PermissionHook";
import {
  Row,
  Col,
  Empty,
  Input,
  Menu,
  Tree,
  Button,
  Divider,
  Tabs,
  Checkbox,
  DatePicker,
  Upload,
  Collapse,
  Switch,
  Form,
  Select,
  InputNumber,
} from "antd";
import serverSideTranslation from "@root/src/helpers/serverSideTranslation";
import {
  SearchOutlined,
  MailOutlined,
  LoadingOutlined,
  PlusOutlined,
  StopOutlined,
} from "@ant-design/icons";
import SubMenu from "antd/lib/menu/SubMenu";
import { useEffect, useState } from "react";
import Search from "antd/lib/transfer/search";
import CameraList from "@root/src/components/User/camera/list";
import FaceFilter from "@root/src/components/User/analysis/faceFilter/filter";
import AnalysisSteps from "@root/src/components/User/analysis/steps/steps";
import VideoList from "@root/src/components/User/video/list";
import moment from "moment";
import CameraTreeSelect from "@root/src/components/User/camera/tree-select";
import FeatureFilter from "@root/src/components/User/analysis/featureFilter/filter";
import AnalysisResult from "@root/src/components/User/analysis/result/result";
import to from "await-to-js";
import deviceService from "@root/src/services/deviceService";
import auth from "@src/helpers/auth";
import UploadImage from "@root/src/components/User/analysis/uploadImage/uploadImage";
import _ from "lodash";

let realtime;

const AnalysisIndex = () => {
  const { checkPermission } = usePermissionHook();
  const { redirect, getStore, setStore, notify } = useBaseHook();
  const dashboardPer = checkPermission({
    dashboard: "R",
  });
  const [searchType, setSearchType] = useState("range");

  const [tab, setTab] = useState("face");
  const [resetCam, setResetCam] = useState(false);
  const [resetVideo, setResetVideo] = useState(false);
  const [loading, setLoading] = useState(false);
  const [startTime, setStartTime] = useState(moment().startOf("day"));
  const [endTime, setEndTime] = useState(moment().endOf("day"));

  const [result, setResult] = useState([]);
  const [resultImage, setResultImage] = useState([]);

  const [channels, setChannels] = useState([]);
  const [total, setTotal] = useState(-1);
  const [totalImage, setTotalImage] = useState(-1);

  const faceFilters = getStore("faceFilters");
  const featureFilters = getStore("featureFilters");

  const faceResult = getStore("faceResult");

  const [imgSearch, setImageSearch] = useState(null);
  const [featureImage, setFeatureImage] = useState(null);
  const [faceImage, setFaceImage] = useState(null);

  const [mode, setMode] = useState("feature");
  const [scanning, setScanning] = useState(false);

  const [scanTime, setScanTime] = useState(10);
  const [finder, setFinder] = useState(null);

  const startScan = async (customTime = null) => {
    if (mode === "image") {
      await scanImage(customTime);
      return;
    }

    setLoading(true);
    let err, res;
    let options = {
      startTime: customTime ? customTime.start : startTime.toISOString(),
      endTime: customTime ? customTime.end : endTime.toISOString(),
      channels: [...channels],
    };

    if (tab === "body") {
      [err, res] = await to(
        deviceService()
          .withAuth()
          .scanFeature({
            ...options,
            filters: featureFilters,
          })
      );
    } else {
      [err, res] = await to(
        deviceService()
          .withAuth()
          .scanFace({
            ...options,
            filters: faceFilters,
          })
      );
    }

    setLoading(false);
    if (err) return console.error("startScan", err);
    auth().setTmpSession(res.session);
    setResult(res.data);
    setTotal(_.isNumber(res.total) ? res.total : -1);
    setFinder(res.finderId || null);
  };

  const scanImage = async (customTime = null) => {
    setLoading(true);
    let [err, res] = await to(
      deviceService()
        .withAuth()
        .scanImage({
          startTime: customTime ? customTime.start : startTime.toISOString(),
          endTime: customTime ? customTime.end : endTime.toISOString(),
          channels: [...channels],
          image: tab === "face" ? faceImage : featureImage,
          type: tab === "body" ? 1 : 0,
        })
    );
    setLoading(false);
    if (err) {
      return notify(err.message, err["code"] ? err["code"] : "", "error");
    }
    console.log("scanImage", res);
    setTotalImage(_.isNumber(res.total) ? res.total : -1);
    setResultImage(res.data);
  };

  const startRealtime = () => {
    // setScanning(true);
    // const scanFn = () => {
    startScan({
      start: moment().subtract(scanTime, "minutes").toISOString(),
      end: moment().toISOString(),
    });
    // };
    // scanFn();
    // realtime = setInterval(() => {
    //   scanFn();
    // }, 5000);
  };

  // const stopRealtime = () => {
  //   setScanning(false);
  //   clearInterval(realtime);
  // };

  // useEffect(() => {
  //   return () => {
  //     stopRealtime();
  //   };
  // }, []);

  const findNext = async () => {
    if (scanning || !finder) return;

    setScanning(true);
    let [err, res] = await to(
      deviceService()
        .withAuth()
        .scanNext({ finderId: finder + "", current: result.length })
    );
    setScanning(false);
    if (!res.total || res.total == 0) {
      setFinder(null);
      return;
    }
    setResult((data) => {
      return [...data, ...res.data];
    });
  };

  return (
    <Row wrap={false}>
      <Col className="sider sider-space" flex={"272px"}>
        <div className="side-container">
          <div className="side-block">
            <Divider className="divider-text">Danh sách camera</Divider>
            <Row>
              <Col span={24}>
                <CameraTreeSelect
                  onChange={(values) => {
                    let chans = values.map((val) => {
                      return Number(val.split("-")[1]);
                    });
                    setChannels(chans);
                  }}
                />
              </Col>
            </Row>
          </div>
          <div className="side-bottom">
            <div style={{ margin: "16px 0" }}>
              <Switch
                onChange={(checked) => {
                  checked ? setSearchType("realtime") : setSearchType("range");
                  // stopRealtime();
                }}
                checkedChildren="Bật"
                unCheckedChildren="Tắt"
              />
              <span
                style={{
                  margin: "0 16px",
                  color: "#f4f4f4",
                }}
              >
                Tìm kiếm nhanh (realtime)
              </span>
            </div>
            <Tabs type="card" activeKey={searchType}>
              <Tabs.TabPane tab="range" key="range">
                <Row gutter={[16, 16]}>
                  <Col span={24}>
                    <DatePicker
                      style={{ width: "100%" }}
                      onChange={(val) => setStartTime(val)}
                      value={startTime}
                      showTime
                    />
                  </Col>
                  <Col span={24}>
                    <DatePicker
                      style={{ width: "100%" }}
                      onChange={(val) => setEndTime(val)}
                      value={endTime}
                      showTime
                    />
                  </Col>
                  <Col span={24}>
                    <Button
                      type="primary"
                      style={{ width: "100%" }}
                      icon={<SearchOutlined />}
                      onClick={() => startScan()}
                      loading={loading}
                      disabled={
                        !channels ||
                        !channels.length ||
                        (mode === "image" &&
                          ((tab === "face" && !faceImage) ||
                            (tab === "body" && !featureImage)))
                      }
                    >
                      Tìm Kiếm Đối Tượng
                    </Button>
                  </Col>
                </Row>
              </Tabs.TabPane>
              <Tabs.TabPane tab="realtime" key="realtime">
                <Row gutter={[16, 16]}>
                  <Col flex="100px">
                    <InputNumber
                      value={scanTime}
                      onChange={(val) => setScanTime(val)}
                      min={5}
                      max={60}
                      style={{ width: "auto" }}
                    />
                  </Col>
                  <Col flex="auto">
                    <Select
                      options={[{ value: "minute", label: "Phút gần nhất" }]}
                      style={{ width: "100%" }}
                      defaultValue={"minute"}
                    ></Select>
                  </Col>
                  <Col span={24}>
                    <Button
                      type="primary"
                      style={{ width: "100%" }}
                      icon={<SearchOutlined />}
                      // onClick={startScan}
                      onClick={startRealtime}
                      loading={loading}
                      disabled={
                        !channels ||
                        !channels.length ||
                        (mode === "image" && !imgSearch)
                      }
                    >
                      Tìm Kiếm Đối Tượng
                    </Button>
                  </Col>
                  {/* <Col span={24}>
                    <Button
                      style={{ width: "100%" }}
                      icon={<StopOutlined />}
                      // onClick={startScan}
                      onClick={stopRealtime}
                      disabled={!scanning}
                    >
                      Dừng
                    </Button>
                  </Col> */}
                </Row>
              </Tabs.TabPane>
            </Tabs>
          </div>
        </div>
      </Col>
      <Col className="container" flex={"auto"}>
        <div className="main-container">
          <AnalysisResult
            data={mode === "feature" ? result : resultImage}
            total={mode === "feature" ? total : totalImage}
            onScan={(url, tab) => {
              setTab(tab);
              const request = new XMLHttpRequest();
              request.open("GET", url, true);
              request.responseType = "blob";
              request.onload = function () {
                var reader = new FileReader();
                reader.readAsDataURL(request.response);
                reader.onload = function (e) {
                  setMode("image");
                  setImageSearch(e.target.result);
                  tab === "face"
                    ? setFaceImage(e.target.result)
                    : setFeatureImage(e.target.result);
                };
              };
              request.send();
            }}
            onLoadMore={findNext}
            type={mode}
            tab={tab}
            loading={loading}
            loadingMore={scanning}
          />
        </div>
      </Col>
      <Col className="sider sider-space" flex={"220px"}>
        <br />
        <Row gutter={[16, 16]}>
          <Col span={12}>
            <Button
              type="primary"
              ghost={tab == "face" ? false : true}
              style={{ width: "100%" }}
              onClick={() => setTab("face")}
            >
              Khuôn mặt
            </Button>
          </Col>
          <Col span={12}>
            <Button
              type="primary"
              style={{ width: "100%" }}
              onClick={() => setTab("body")}
              ghost={tab == "body" ? false : true}
            >
              Ngoại hình
            </Button>
          </Col>
          {/* <Col span={24}>
            <Button
              type="primary"
              style={{ width: "100%" }}
              onClick={() => setTab("upload")}
              ghost={tab == "upload" ? false : true}
            >
              Tải hình ảnh
            </Button>
          </Col> */}
        </Row>
        <br />
        <Collapse activeKey={[mode]} ghost accordion expandIcon={() => <></>}>
          <Collapse.Panel
            header={
              <Divider className="divider-text">
                <Button
                  size="small"
                  shape="round"
                  ghost={mode === "image" ? false : true}
                  type="primary"
                  onClick={() =>
                    setMode(mode === "image" ? "feature" : "image")
                  }
                >
                  Chọn hình ảnh
                </Button>
              </Divider>
            }
            key="image"
          >
            <div>
              <UploadImage
                onUpload={(img) => {
                  setImageSearch(img);
                  tab === "face" ? setFaceImage(img) : setFeatureImage(img);
                }}
                img={tab === "face" ? faceImage : featureImage}
                onClear={() => {
                  setImageSearch(null);
                  tab === "face" ? setFaceImage(null) : setFeatureImage(null);
                }}
              />
            </div>
          </Collapse.Panel>
          <Collapse.Panel
            header={
              <Divider className="divider-text">
                <Button
                  size="small"
                  shape="round"
                  ghost={mode === "feature" ? false : true}
                  type="primary"
                  onClick={() =>
                    setMode(mode === "feature" ? "image" : "feature")
                  }
                >
                  Chọn đặc điểm
                </Button>
              </Divider>
            }
            key="feature"
          >
            <Row>
              <Col span={24}>
                <Tabs activeKey={tab}>
                  <Tabs.TabPane key="face">
                    <FaceFilter />
                  </Tabs.TabPane>
                  <Tabs.TabPane key="body">
                    <FeatureFilter />
                  </Tabs.TabPane>
                  <Tabs.TabPane key="upload">
                    <Upload
                      name="avatar"
                      listType="picture-card"
                      className="avatar-uploader"
                      showUploadList={false}
                      // beforeUpload={beforeUpload}
                      accept="image/*"
                    >
                      {false && !loading ? (
                        <img
                          src={"preview"}
                          alt="preview"
                          style={{ width: "100%", height: "100%" }}
                        />
                      ) : (
                        <div>
                          {loading ? <LoadingOutlined /> : <PlusOutlined />}
                          <div style={{ marginTop: 8 }}>Tải ảnh </div>
                        </div>
                      )}
                    </Upload>
                  </Tabs.TabPane>
                </Tabs>
              </Col>
            </Row>
          </Collapse.Panel>
        </Collapse>
      </Col>
    </Row>
  );
};

AnalysisIndex.Layout = (props) => {
  const { t } = useBaseHook();
  return (
    <Layout
      title={t("pages:analysis.index.title")}
      description={t("pages:analysis.index.description")}
      {...props}
    />
  );
};

export const getStaticProps = async (context) => {
  return {
    props: {
      ...(await serverSideTranslation(context)),
    },
  };
};

export default AnalysisIndex;
