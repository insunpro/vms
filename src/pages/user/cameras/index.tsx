import dynamic from "next/dynamic";
const Layout = dynamic(() => import("@src/layouts/User"), { ssr: false });
import useBaseHooks from "@src/hooks/BaseHook";
import usePermissionHook from "@src/hooks/PermissionHook";
import {
  Row,
  Col,
  Empty,
  Input,
  Button,
  Tooltip,
  Divider,
  Card,
  Typography,
  Form,
  Select,
} from "antd";
import serverSideTranslation from "@root/src/helpers/serverSideTranslation";
import {
  SearchOutlined,
  FullscreenExitOutlined,
  FullscreenOutlined,
} from "@ant-design/icons";
import ReactPlayer from "react-player";
import { useEffect, useRef, useState } from "react";
import cameras from "@root/public/static/cameras.json";
import VideoPlayer from "@root/src/components/User/video/player";
import CameraList from "@root/src/components/User/camera/list";
import _ from "lodash";
import DeviceTree from "@root/src/components/User/camera/tree-select";
import CameraTreeClick from "@root/src/components/User/camera/tree-click";
import WebRtcCam from "@root/src/components/User/players/WebRtcCam";
import { useAsyncEffect, useFullscreen } from "ahooks";
import PeerPlayer from "@root/src/components/User/players/PeerPlayer";
import P2PService from "@root/src/services/p2p/service";

const grids = [
  "",
  "ant-col-24",
  "ant-col-12",
  "ant-col-8",
  "ant-col-6",
  "ant-col-55",
  "ant-col-4",
];

const CameraIndex = () => {
  const { t, notify, setStore, getStore } = useBaseHooks();
  const isMobile = getStore("isMobile");
  const [cams, setCams] = useState([]);
  const [col, setCol] = useState(3);
  const [row, setRow] = useState(2);
  const [pc, setPc] = useState(null);

  const currentViews = getStore("currentViews") || [];
  const ref = useRef(null);
  const [isFullscreen, { enterFullscreen, exitFullscreen, toggleFullscreen }] =
    useFullscreen(ref);

  const renderCameras = () => {
    const SRCS = ["/videos/vms-sample-1.mp4"];
    let maxScreens = row * col;
    let screens = [];
    for (let i = 0; i < maxScreens; i++) {
      screens.push(
        <Col className={`${grids[col]}`} key={i}>
          <div className="player-grid">
            {currentViews[i] && (
              // <WebRtcCam cameraHost={`cam-${currentViews[i]}`} />
              <PeerPlayer peer={pc} cameraId={`cam-${currentViews[i]}`} />
            )}
          </div>
        </Col>
      );
    }
    return screens;
  };

  useEffect(() => {
    let views = [];
    let max = row * col;
    if (cameras.RECORDS.length > max) {
      views = [..._.cloneDeep(cameras.RECORDS)].splice(0, max);
    } else views = [...cameras.RECORDS];
    // setTimeout(() => setCams(views), 200);
  }, [row, col]);

  useAsyncEffect(async () => {
    // setStore("currentViews", []);

    let peerConnection = await P2PService.getConnection("mq84", (err, res) => {
      console.log(">>>>>>>>>>err:", err);
      console.log(">>>>>>>>>>err:", res);
    });
    setPc(peerConnection);
  }, []);

  return (
    <Row wrap={false}>
      <Col className="sider sider-space" flex={"272px"}>
        <div className="side-container">
          <div className="side-block">
            <Divider className="divider-text">Danh sách camera</Divider>
            <Row>
              <Col span={24}>
                {/* <CameraList /> */}
                <CameraTreeClick />
              </Col>
            </Row>
          </div>
          <div className="side-bottom">
            <Form
              layout="horizontal"
              initialValues={{
                row: row,
                col: col,
              }}
              labelCol={{ span: 8 }}
              wrapperCol={{ span: 16 }}
            >
              <Form.Item name="row" label="Số hàng">
                <Select
                  options={[
                    { label: 1, value: 1 },
                    { label: 2, value: 2 },
                    { label: 3, value: 3 },
                  ]}
                  onChange={(value) => setRow(Number(value))}
                ></Select>
              </Form.Item>
              <Form.Item name="col" label="Số cột">
                <Select
                  options={[
                    { label: 1, value: 1 },
                    { label: 2, value: 2 },
                    { label: 3, value: 3 },
                    { label: 4, value: 4 },
                    { label: 5, value: 5 },
                  ]}
                  onChange={(value) => setCol(Number(value))}
                ></Select>
              </Form.Item>
            </Form>
          </div>
        </div>
      </Col>
      <Col className="container" flex={"auto"}>
        <div className="main-container">
          <div ref={ref}>
            <Card
              className="card-container"
              title={
                <Row>
                  <Col span={12}>Trực tiếp</Col>
                  <Col span={12}>
                    <div style={{ float: "right" }}>
                      {!isFullscreen ? (
                        <Tooltip title="Toàn màn hình">
                          <Button
                            icon={<FullscreenOutlined />}
                            size="small"
                            ghost
                            type="primary"
                            onClick={enterFullscreen}
                          ></Button>
                        </Tooltip>
                      ) : (
                        <Tooltip title="Đóng">
                          <Button
                            icon={<FullscreenExitOutlined />}
                            size="small"
                            ghost
                            type="primary"
                            onClick={exitFullscreen}
                          ></Button>
                        </Tooltip>
                      )}
                    </div>
                  </Col>
                </Row>
              }
            >
              <Row gutter={[8, 8]} justify="center">
                {renderCameras()}
              </Row>
            </Card>
          </div>
        </div>
      </Col>
    </Row>
  );
};

CameraIndex.Layout = (props) => {
  const { t } = useBaseHooks();
  return (
    <Layout
      title={t("pages:cameras.index.title")}
      description={t("pages:cameras.index.description")}
      {...props}
    />
  );
};

export const getStaticProps = async (context) => {
  return {
    props: {
      ...(await serverSideTranslation(context)),
    },
  };
};

export default CameraIndex;
