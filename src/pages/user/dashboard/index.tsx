import dynamic from "next/dynamic";
const Layout = dynamic(() => import("@src/layouts/User"), { ssr: false });
import useBaseHook from "@src/hooks/BaseHook";
import usePermissionHook from "@src/hooks/PermissionHook";
import {
  Row,
  Col,
  Empty,
  Input,
  Button,
  Typography,
  Divider,
  Progress,
} from "antd";
import serverSideTranslation from "@root/src/helpers/serverSideTranslation";
import { SearchOutlined, PlusOutlined } from "@ant-design/icons";
import TechnicalDrawing from "@root/src/components/User/dashboard/technicalDrawing";
import cameras from "@root/public/static/cameras.json";
import StreamModal from "@root/src/components/User/modal/StreamModal";
import { useEffect, useState } from "react";
import LayoutList from "@root/src/components/User/dashboard/listLayouts";
import ActiveNumber from "@root/src/components/User/dashboard/activeNumber";

const Dashboard = () => {
  const [visible, setVisible] = useState(false);

  const CONFIG = {
    STATUS_CLASS: {
      0: "offline",
      1: "online",
      2: "alert",
    },
    STATUS_TEXT: {
      0: "Không Hoạt động",
      1: "Hoạt động",
      2: "Cảnh báo",
    },
  };
  const convertToMarkers = (cams) => {
    let markers = cams.map((cam) => {
      let position = JSON.parse(cam.position);
      return {
        ...position,
        type: cam.type,
        statusClass: CONFIG.STATUS_CLASS[cam.status],
        statusText: CONFIG.STATUS_TEXT[cam.status],
        values: cam,
      };
    });
    return markers;
  };

  return (
    <Row wrap={false}>
      <Col className="sider sider-space" flex={"272px"}>
        <div className="side-container">
          <div className="side-block">
            <Divider className="divider-text">Danh sách khu vực</Divider>
            <LayoutList />
          </div>
          <div className="side-bottom-small">
            <Row>
              <Button
                type="primary"
                style={{ width: "100%", marginTop: 40 }}
                icon={<PlusOutlined />}
              >
                Thêm Khu Vực
              </Button>
            </Row>
          </div>
        </div>
      </Col>
      <Col className="container" flex={"auto"}>
        <Row justify="center" style={{ marginTop: 24 }}>
          <Col xs={24} sm={24} md={18}>
            <div style={{ textAlign: "center" }}>
              <Typography.Title type="warning">
                Sơ đồ lắp đặt Khu vực 1
              </Typography.Title>
            </div>
            <TechnicalDrawing
              markers={convertToMarkers(cameras.RECORDS)}
              onClick={() => setVisible(true)}
            />
          </Col>
        </Row>
      </Col>
      <Col className="sider sider-space" flex={"272px"}>
        <Divider className="divider-text">Trạng thái</Divider>
        <ActiveNumber maxNumber={cameras.RECORDS.length} />
        <Divider className="divider-text">Danh sách cảnh báo</Divider>
      </Col>
      <StreamModal
        camera={{ host: "asdasd" }}
        visible={visible}
        onClose={() => setVisible(false)}
      />
    </Row>
  );
};

Dashboard.Layout = (props) => {
  const { t } = useBaseHook();
  return (
    <Layout
      title={t("pages:dashboard.index.title")}
      description={t("pages:dashboard.index.description")}
      {...props}
    />
  );
};

export const getStaticProps = async (context) => {
  return {
    props: {
      ...(await serverSideTranslation(context)),
    },
  };
};

export default Dashboard;
