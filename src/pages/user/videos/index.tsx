import dynamic from "next/dynamic";
const Layout = dynamic(() => import("@src/layouts/User"), { ssr: false });
import useBaseHook from "@src/hooks/BaseHook";
import usePermissionHook from "@src/hooks/PermissionHook";
import {
  Row,
  Col,
  Empty,
  Input,
  Button,
  Divider,
  Form,
  Card,
  TimePicker,
  DatePicker,
} from "antd";
import serverSideTranslation from "@root/src/helpers/serverSideTranslation";
import { SearchOutlined, PlusOutlined } from "@ant-design/icons";
import VideoForm from "@root/src/components/User/video/form";
import { useEffect, useState } from "react";
import { addQuery } from "@root/src/helpers/routes";
import VideoGrid from "@root/src/components/User/video/grid";
import CameraTreeSelect from "@root/src/components/User/camera/tree-select";
import moment from "moment-timezone";
import VideoResult from "@root/src/components/User/video/result";
import auth from "@root/src/helpers/auth";
import deviceService from "@root/src/services/deviceService";
import to from "await-to-js";

const VideoIndex = () => {
  const { checkPermission } = usePermissionHook();
  const { redirect, router } = useBaseHook();

  const [tab, setTab] = useState("video");
  const [channels, setChannels] = useState([]);
  const [loading, setLoading] = useState(false);
  const [startTime, setStartTime] = useState(moment().startOf("day"));
  const [endTime, setEndTime] = useState(moment().endOf("day"));

  const [result, setResult] = useState([]);
  const [total, setTotal] = useState(-1);

  const startScan = async () => {
    setLoading(true);
    let err, res;
    if (tab === "video") {
      [err, res] = await to(
        deviceService()
          .withAuth()
          .scanVideo({
            startTime: startTime.toISOString(),
            endTime: endTime.toISOString(),
            channels: [...channels],
          })
      );
    } else {
      // [err, res] = await to(
      //   deviceService()
      //     .withAuth()
      //     .scanFace({
      //       startTime: startTime.toISOString(),
      //       endTime: endTime.toISOString(),
      //       channels: [...channels],
      //       filters: faceFilters,
      //     })
      // );
    }

    setLoading(false);
    if (err) return console.error("startScan", err);
    auth().setTmpSession(res.session);
    setResult(res.data);
    setTotal(res.total);
  };

  return (
    <Row>
      <Col className="sider sider-space" flex={"272px"}>
        <div className="side-container">
          <div className="side-block">
            <Divider className="divider-text">Danh sách Camera</Divider>
            <Row>
              <CameraTreeSelect
                onChange={(values) => {
                  let chans = values.map((val) => {
                    return Number(val.split("-")[1]);
                  });
                  setChannels(chans);
                }}
              />
            </Row>
          </div>
          <div className="side-bottom">
            <Row gutter={[16, 16]}>
              <Col span={24}>
                <DatePicker
                  style={{ width: "100%" }}
                  onChange={(val) => setStartTime(val)}
                  value={startTime}
                  showTime
                />
              </Col>
              <Col span={24}>
                <DatePicker
                  style={{ width: "100%" }}
                  onChange={(val) => setEndTime(val)}
                  value={endTime}
                  showTime
                />
              </Col>
              <Col span={24}>
                <Button
                  type="primary"
                  style={{ width: "100%" }}
                  icon={<SearchOutlined />}
                  onClick={startScan}
                  loading={loading}
                  disabled={!channels || !channels.length}
                >
                  Tìm Kiếm Video
                </Button>
              </Col>
            </Row>
          </div>
        </div>
      </Col>
      <Col className="container" flex={"1"}>
        <div className="main-container">
          <VideoResult data={result} total={total} loading={loading} />
        </div>
      </Col>
    </Row>
  );
};

VideoIndex.Layout = (props) => {
  const { t } = useBaseHook();
  return (
    <Layout
      title={t("pages:videos.index.title")}
      description={t("pages:videos.index.description")}
      {...props}
    />
  );
};

export const getStaticProps = async (context) => {
  return {
    props: {
      ...(await serverSideTranslation(context)),
    },
  };
};

export default VideoIndex;
