import Base from "./baseService";
import auth from "@src/helpers/auth";

class AnalysisService extends Base {
  analysis = async (data) => {
    return this.request({
      url: "/api/v1/analysis/find",
      method: "POST",
      data: data,
    });
  };

  smartVideo = async (data) => {
    return this.request({
      url: "/api/v1/analysis/smart",
      method: "POST",
      data: data,
    });
  };

  smartPhoto = async (data) => {
    return this.request({
      url: "/api/v1/analysis/photo",
      method: "POST",
      data: data,
      options: {
        allowUpload: true,
      },
    });
  };
}

export default () => new AnalysisService();
