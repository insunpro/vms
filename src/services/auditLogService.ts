import Base from "./baseService";

class AuditLogService extends Base {
  index = async (filter: any) => {
    return this.request({
      url: "/api/v1/auditLogs",
      method: "GET",
      data: filter,
    });
  };
}

export default () => new AuditLogService();
