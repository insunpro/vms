import getConfig from "next/config";
import UrlPattern from "url-pattern";
import moment from "moment-timezone";
const { publicRuntimeConfig } = getConfig();

import auth, { AuthInterface } from "@src/helpers/auth";
class BaseService {
  auth: AuthInterface;
  withAuth = (context?: any) => {
    this.auth = auth(context);
    return this;
  };

  makeQuery(data = {}) {
    let query = [];
    for (let key in data) {
      if (Array.isArray(data[key])) {
        for (let value of data[key]) {
          if (typeof value == "object") value = JSON.stringify(value);
          query.push({ key: `${key}[]`, value: value });
        }
      } else if (typeof data[key] == "object") {
        query.push({ key: key, value: JSON.stringify(data[key]) });
      } else {
        query.push({ key: key, value: data[key] });
      }
    }
    return query.map((q) => `${q.key}=${q.value}`).join("&");
  }

  request = async ({
    url,
    method,
    data,
    options,
  }: {
    url: string;
    method: "GET" | "POST" | "PUT" | "DELETE";
    data?: any;
    options?: any;
  }) => {
    if (["GET", "POST", "PUT", "DELETE"].includes(method)) {
      let pattern = new UrlPattern(url);
      let asUrl = pattern.stringify(data);
      url = asUrl;
    }
    if (["GET", "DELETE"].includes(method)) {
      url += "?" + this.makeQuery(data);
    } else {
      if (options?.allowUpload) {
        const formData = new FormData();
        this.buildFormData(formData, data);
        data = formData;
      } else {
        data = JSON.stringify(data);
      }
      options = {
        ...options,
        body: data,
      };
    }

    let requestOptions = {
      method: method,
      headers: options?.allowUpload
        ? {}
        : {
            "Content-Type": "application/json",
          },
      ...options,
    };

    delete requestOptions.allowUpload;

    if (this.auth && this.auth.token) {
      requestOptions.headers["Authorization"] = `Bearer ${this.auth.token}`;
      requestOptions.headers["TimeZone"] = moment.tz.guess();
    }

    const result = await fetch(
      publicRuntimeConfig.API_HOST + url,
      requestOptions
    );

    return await this.handleResponse(result, options);
  };

  handleResponse = async (response, options) => {
    let data: any = {};

    if (options && options.download) {
      data = response.blob();
    } else {
      const text = await response.text();
      try {
        data = text && JSON.parse(text);
      } catch (e) {
        console.log(e);
      }
    }

    if (!response.ok) {
      if ([401].indexOf(response.status) !== -1) {
        // auto logout if 401 Unauthorized or 403 Forbidden response returned from api
        auth().logout();
        location.href = "/login";
      }
      const error = data || (data && data.message) || response.statusText;
      return Promise.reject(error);
    }

    return data.data || data;
  };

  buildFormData(formData, data, parentKey = null) {
    if (
      data &&
      typeof data === "object" &&
      !(data instanceof Date) &&
      !(data instanceof File) &&
      !(data instanceof moment)
    ) {
      Object.keys(data).forEach((key) => {
        this.buildFormData(
          formData,
          data[key],
          parentKey ? `${parentKey}[${key}]` : key
        );
      });
    } else {
      let value = data == null ? "" : data;
      if (data instanceof moment) {
        //@ts-ignore
        value = data.toISOString();
      }

      formData.append(parentKey, value);
    }
  }
}

export default BaseService;
