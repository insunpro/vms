import Base from "./baseService";

class DeviceService extends Base {
  scanFace = async (data: any) => {
    return this.request({
      url: "/api/v1/scan/face",
      method: "POST",
      data: data,
    });
  };

  scanFeature = async (data: any) => {
    return this.request({
      url: "/api/v1/scan/feature",
      method: "POST",
      data: data,
    });
  };

  scanVideo = async (data: any) => {
    return this.request({
      url: "/api/v1/scan/video",
      method: "POST",
      data: data,
    });
  };

  scanImage = async (data: any) => {
    return this.request({
      url: "/api/v1/scan/image",
      method: "POST",
      data: data,
    });
  };

  scanNext = async (data: any) => {
    return this.request({
      url: "/api/v1/scan/next",
      method: "POST",
      data: data,
    });
  };

  getPlayback = async (data: any) => {
    return this.request({
      url: "/api/v1/device/playback",
      method: "GET",
      data: data,
    });
  };

  getDevice = async (data: any) => {
    return this.request({
      url: "/api/v1/device/list",
      method: "GET",
      data: data,
    });
  };

  webrtcSdp = async (data: any) => {
    return this.request({
      url: "/api/v1/webrtc/sdp",
      method: "GET",
      data: data,
    });
  };

  webrtcCodec = async (data: any) => {
    return this.request({
      url: "/api/v1/webrtc/codec",
      method: "GET",
      data: data,
    });
  };
}

export default () => new DeviceService();
