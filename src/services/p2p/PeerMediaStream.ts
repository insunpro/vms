import { log } from "./log";

class PeerMediaStream {
  private stream;
  private callbackFn;
  private connecting = false;
  private options = {
    bandwidth: "20",
    timeout: 10,
  };

  private config = {
    iceServers: [
      {
        urls: ["stun:stun.l.google.com:19302"],
      },
    ],
  }; // peer config

  /**
   * stream: Peerconnection after dial stream server.
   * config: iceServers Config
   * options: options for mediastream
   */
  constructor({ stream, config, options }) {
    this.stream = stream;
    if (config) {
      this.config = {
        ...this.config,
        ...config,
      };
    }
    if (options) {
      this.options = {
        ...this.options,
        ...options,
      };
    }

    stream.on("data", (chunk) => {
      try {
        const data = JSON.parse(chunk);
        //p.signal(data.answer);
        if (typeof this.callbackFn == "function") {
          this.callbackFn(data);
          this.callbackFn = undefined;
        }
      } catch (e) {
        console.log(e);
        throw e;
      }
    });
    stream.on("error", (err) => {
      log("ERR", err);
    });
  }

  static connect = async ({
    peer,
    streamServer = "127.0.0.1:6001",
    cameraId,
    // videoElement,
    options = {
      bandwidth: "20",
      timeout: 10,
    },
  }) => {
    const stream = peer.dial(streamServer);

    const peerMediaStream = new PeerMediaStream({
      stream,
      config: peer.config,
      options,
    });

    let videoStream =  await peerMediaStream.connect({
      // videoElement: videoElement,
      cameraId: cameraId,
      timeout: options.timeout,
    });
    stream.destroy();

    return videoStream
  };

  async connect({ cameraId, timeout = 10 }) {
    if (this.connecting) {
      log(`Stream in process. Please wait...`);
      return;
    }

    this.connecting = true;

    return new Promise<MediaStream>((resolve, reject) => {
      log(`start connect media peer ...`);

      const waitToCompleteIceGathering = async (pc) => {
        return new Promise((resolve) => {
          pc.addEventListener(
            "icegatheringstatechange",
            (e) =>
              e.target.iceGatheringState === "complete" &&
              resolve(pc.localDescription)
          );
          pc.addEventListener("onicecandidate", (e) => {
            e.candidate == null && resolve(pc.localDescription);
          });
        });
      };

      const updateBandwidth = () => {
        const desc = {
          type: pc.localDescription.type,
          sdp:
            this.options.bandwidth === "unlimited"
              ? this.removeBandwidthRestriction(pc.localDescription.sdp)
              : this.updateBandwidthRestriction(
                  pc.localDescription.sdp,
                  this.options.bandwidth
                ),
        };
        log(
          "Applying bandwidth restriction to setRemoteDescription:\n" + desc.sdp
        );

        pc.setLocalDescription(desc);

        const sender = pc.getSenders()[0];
        const parameters = sender.getParameters();
        if (!parameters.encodings) {
          parameters.encodings = [{}];
        }
        if (this.options.bandwidth === "unlimited") {
          delete parameters.encodings[0].maxBitrate;
        } else {
          parameters.encodings[0].maxBitrate =
            Number(this.options.bandwidth) * 1000;
        }
        sender.setParameters(parameters).catch((e) => console.error(e));
        return;
      };

      const pc = new RTCPeerConnection(this.config);
      pc.addTransceiver("video", {
        direction: "sendrecv",
      });

      //   pc.onicecandidate = (e) => {
      //     log("onicecandidate", e)
      //   };

      pc.onnegotiationneeded = async () => {
        let offer = await pc.createOffer();
        await pc.setLocalDescription(offer);

        log("wait ice Candidates....");
        await waitToCompleteIceGathering(pc);
        updateBandwidth();

        log("sent offer", pc.localDescription);
        this.callbackFn = (data) => {
          log(`Recv answer`, data);
          this.connecting = false;

          if (data.error) {
            console.log(data.message);
            return;
          }
          if (this.options.bandwidth != "unlimited") {
            pc.setRemoteDescription({
              type: data.answer.type,
              sdp: this.updateBandwidthRestriction(
                data.answer.sdp,
                this.options.bandwidth
              ),
            });
          } else {
            pc.setRemoteDescription(data.answer);
          }
        };

        this.stream.write(
          JSON.stringify({
            method: "connectWRTC",
            cameraId: cameraId,
            offer: pc.localDescription,
            iceServers: this.config.iceServers,
          })
        );
      };
      let stream = new MediaStream();

      pc.ontrack = (event) => {
        log("on track", event);
        stream.addTrack(event.track);
        // videoElement.srcObject = stream;
        // videoElement.play();
        //   log(event.streams.length + ' track is delivered')
        resolve(stream);
      };

      pc.onconnectionstatechange = (event) => {
        switch(pc.connectionState) {
          case "connected":
            // The connection has become fully connected
            log("on connection state change", "connected");

            break;
          case "disconnected":
            log("on connection state change", "disconnected");
            break;
          case "failed":
            // One or more transports has terminated unexpectedly or in an error
            log("on connection state change", "failed");
            break;
          case "closed":
            // The connection has been closed
            log("on connection state change", "closed");
            break;
        }
      };

      // connection timeout
      setTimeout(() => {
        reject("connection timeout");
      }, timeout * 1000);
    });
  }

  updateBandwidthRestriction = (sdp, bandwidth) => {
    let modifier = "AS";
    const isFirefox =
      typeof window != "undefined" && window.navigator
        ? window.navigator.userAgent.toLowerCase().indexOf("firefox") > -1
        : false;

    if (isFirefox) {
      bandwidth = (bandwidth >>> 0) * 1000;
      modifier = "TIAS";
    }
    if (sdp.indexOf("b=" + modifier + ":") === -1) {
      // insert b= after c= line.
      sdp = sdp.replace(
        /c=IN (.*)\r\n/,
        "c=IN $1\r\nb=" + modifier + ":" + bandwidth + "\r\n"
      );
    } else {
      sdp = sdp.replace(
        new RegExp("b=" + modifier + ":.*\r\n"),
        "b=" + modifier + ":" + bandwidth + "\r\n"
      );
    }
    return sdp;
  };

  removeBandwidthRestriction = (sdp) => {
    return sdp.replace(/b=AS:.*\r\n/, "").replace(/b=TIAS:.*\r\n/, "");
  };
}

export default PeerMediaStream;
module.exports = PeerMediaStream;
