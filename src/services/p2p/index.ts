// if (!process.env.IS_TS_NODE) {
//     // tslint:disable-next-line:no-var-requires
//     require('module-alias/register');
// }
import Peer from "simple-peer";
// import net from "net";

import wrtc from "wrtc";
import Multiplex from "multiplex";
import { log } from "./log";
import PeerMediaStream from "./PeerMediaStream";
import EventEmitter from "events";

interface PeerConnectionInterface {
  server: string;
  peerId: string;
  agentId: string;
  config?: any;
}
class PeerConnection extends EventEmitter {
  private serverUrl;
  private peerId;
  private agentId;
  private permissions = [];
  private peer;
  private muxer;
  private connections = [];
  public connected = false;
  private defaultConfig = {
    iceServers: [
      {
        urls: ["stun:stun.l.google.com:19302"],
      },
    ],
    // "bundlePolicy": "max-bundle",
    // "iceCandidatePoolSize": 1
  };
  public config = {};

  constructor({ server, peerId, agentId, config }: PeerConnectionInterface) {
    super();
    this.serverUrl = server;
    this.peerId = peerId; //id cuar nguowif muon ket noi
    this.agentId = agentId; //id cuar minhf
    if (config) {
      this.config = {
        ...this.config,
        ...config,
      };
    }
  }

  public loadIceConfig = async () => {
    console.log(`Load ice config from server...`);
    const response = await fetch(this.serverUrl + "/auth", {
      method: "post",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({
        agentId: this.agentId,
      }),
    });

    console.log(`SUCCESS Load ice config from server...`);

    const { success, message, iceConfig } = (await response.json()) as any;
    if (!success) {
      console.log("[Auth] Server response ERR:", message);

      this.emit("error", {
        type: "auth",
        message: message,
      });

      return {};
    }

    this.config = {
      ...this.defaultConfig,
      ...iceConfig,
      ...this.config,
    };

    console.log("Auth success. iceConfig:", iceConfig);
    this.emit("auth", iceConfig);
  };

  public createConnection = async () => {
    await this.loadIceConfig();
    this.muxer = new Multiplex();
    console.log(`Create peer connection`);
    this.peer = new Peer({
      initiator: true,
      wrtc: wrtc,
      trickle: false,
      config: this.config,
    });

    this.peer.on("error", (e) => {
      console.log(`Peer Error: `, e);

      this.emit("error", e);
      this.muxer.destroy();
      this.connections.map((conn) => conn.destroy());
      this.connected = false;
    });

    this.peer.on("close", () => {
      console.log(`Peer Closed`);

      this.emit("close");
      this.muxer.destroy();
      this.connections.map((conn) => conn.destroy());
      this.connected = false;
    });

    const sdp = await new Promise((r) =>
      this.peer.on("signal", (data) => {
        r(data);
      })
    );

    console.log(`send offer to server....`);
    const response = await fetch(this.serverUrl + "/new-peer", {
      method: "post",
      body: JSON.stringify({
        agentId: this.agentId,
        peerId: this.peerId,
        offerSDP: sdp,
      }),
      headers: { "Content-Type": "application/json" },
    });

    const { success, message, answerSDP, permissions } =
      (await response.json()) as any;
    if (!success) {
      this.emit("error", { type: "offer", message });

      console.log("Server response ERR:", message);
      return {};
    }

    this.emit("answer", { permissions, answerSDP });

    console.log(`Server Response success`, permissions, answerSDP);

    this.permissions = permissions;
    this.peer.signal(answerSDP);

    await new Promise<void>((r) => {
      this.peer.on("connect", () => {
        console.log(`Peer success. PeerId: ${this.peerId}`);

        this.emit("connect", {
          peerId: this.peerId,
        });
        this.peer.pipe(this.muxer).pipe(this.peer);
        r();
      });
    });

    this.connected = true;
    return {
      connection: this.peer,
      permissions,
      connected: this.connected,
    };
  };

  public dial = (protocol) => {
    if (!this.connected) {
      console.log(`Peer is not connected.`);
      return;
    }
    console.log("Dial to protocol successfully. Return Stream!");

    const stream = this.muxer.createStream(protocol);
    this.emit("dial", { stream });
    return stream;
  };

  // public dialWithPort = async ({ protocol, host, port }: any) => {
  //   if (!this.connected) {
  //     console.log(`Peer is not connected.`);
  //     return;
  //   }

  //   if (!host) host = "0.0.0.0";
  //   const conn = net.createServer((socket) => {
  //     const remoteStream = this.dial(protocol);
  //     socket.pipe(remoteStream).pipe(socket);
  //   });
  //   (async () => {
  //     conn.listen(
  //       {
  //         port,
  //         host,
  //       },
  //       () => {
  //         this.connections.push(conn);
  //         log(
  //           "Dial to protocol (with port) successfully. Listen: ",
  //           host,
  //           port
  //         );
  //       }
  //     );
  //   })();

  //   return conn;
  // };
}

PeerConnection["PeerMediaStream"] = PeerMediaStream;
export default PeerConnection;
export { PeerMediaStream };
module.exports = PeerConnection;
