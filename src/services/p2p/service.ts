import connection from "@root/server/libs/core/Databases";
import to from "await-to-js";
import PeerConnection from ".";

class P2PService {
  private static connections = {};
  static server = "https://p2p.demo.mqsolutions.vn";
  static agentId = "web-client-dev-" + Math.round(Math.random() * 1000000);

  static async getConnection(peerId, callback) {
    if (this.connections[peerId]) {
      return this.connections[peerId] || null;
    } else return await this.createConnection(peerId, callback);
  }

  static async createConnection(peerId, callback) {
    console.log({ server: this.server, peerId: peerId, agentId: this.agentId });

    const pc = new PeerConnection({
      server: this.server,
      peerId: peerId,
      agentId: this.agentId,
    });

    const [err, res] = await to(pc.createConnection());
    if (!res || !res.connected) {
      callback && callback(err, res);
    }
    this.connections[peerId] = pc;
    return pc;
  }
}

export default P2PService;
